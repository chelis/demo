from django.test import TestCase

from ui.models import EnvironmentGeom, BorderGeom, Field
from ui.business.managers import *


class ImporterTests(TestCase):
    """Tests import of shape files and other geometry files"""

    fixtures = ['test_data.json']

    def test_run_bucket(self):
        importer = Importer()
        EnvironmentGeom.objects.filter(productive_unit=4).delete()
        self.assertEqual(EnvironmentGeom.objects.filter(productive_unit=4).count(), 0)

        importer.run('ui/testsresources/parcelas.zip',
                     Field.objects.get(pk=4), EnvironmentGeom)

        self.assertEqual(EnvironmentGeom.objects.filter(productive_unit=4).count(), 48)

        importer.run('ui/testsresources/contornos.zip',
                     Field.objects.get(pk=4), EnvironmentGeom)

        self.assertEqual(EnvironmentGeom.objects.filter(productive_unit=4).count(), 1)

    def test_run_borders(self):
        importer = Importer()
        BorderGeom.objects.filter(productive_unit=4).delete()
        self.assertEqual(BorderGeom.objects.filter(productive_unit=4).count(), 0)

        importer.run('ui/testsresources/parcelas.zip',
                     Field.objects.get(pk=4), BorderGeom)

        self.assertEqual(BorderGeom.objects.filter(productive_unit=4).count(), 48)

        importer.run('ui/testsresources/contornos.zip',
                     Field.objects.get(pk=4), BorderGeom)

        self.assertEqual(BorderGeom.objects.filter(productive_unit=4).count(), 1)

    def test_run_bucket_info(self):
        importer = Importer()
        EnvironmentGeom.objects.filter(productive_unit=4).delete()
        self.assertEqual(EnvironmentGeom.objects.filter(productive_unit=4).count(), 0)

        importer.run('ui/testsresources/parcelas.zip',
                     Field.objects.get(pk=4), EnvironmentGeom)

        buckets = EnvironmentGeom.objects.filter(productive_unit=4)

        self.assertEqual(sorted(buckets[0].info.keys()),
                         sorted([u'NDVI_STD', u'Variedad', u'Id_Varieda', u'Indice_Fen',
                                u'NDVI_medio', u'Repeticion', u'NDVI_STD_dt',
                                u'Variedad_dt', u'Id_Varieda_dt', u'Indice_Fen_dt',
                                u'NDVI_medio_dt', u'Repeticion_dt']))

    def test_run_borders_info(self):
        importer = Importer()
        BorderGeom.objects.filter(productive_unit=4).delete()
        self.assertEqual(BorderGeom.objects.filter(productive_unit=4).count(), 0)

        importer.run('ui/testsresources/parcelas.zip', Field.objects.get(pk=4),
                     BorderGeom)
        buckets = BorderGeom.objects.filter(productive_unit=4).order_by('id')

        self.assertEqual(sorted(buckets[0].info.keys()),
                         sorted([u'NDVI_STD', u'Variedad', u'Id_Varieda',
                                u'Indice_Fen', u'NDVI_medio', u'Repeticion',
                                u'NDVI_STD_dt', u'Variedad_dt', u'Id_Varieda_dt',
                                u'Indice_Fen_dt', u'NDVI_medio_dt', u'Repeticion_dt']))

        self.assertEqual(buckets[0].info['NDVI_medio'], '0.745245586918')
        self.assertEqual(buckets[0].info['NDVI_STD'], '0.113153639924')
        self.assertEqual(buckets[0].info['Repeticion'], '4')
        self.assertEqual(buckets[0].info['Variedad'], 'Shuffle')
        self.assertEqual(buckets[0].info['Id_Varieda'], '4')
        self.assertEqual(buckets[0].info['Indice_Fen'], '4.9')

        self.assertEqual(buckets[47].info['NDVI_medio'], '0.722614254762')
        self.assertEqual(buckets[47].info['NDVI_STD'], '0.0972468374496')
        self.assertEqual(buckets[47].info['Repeticion'], '1')
        self.assertEqual(buckets[47].info['Variedad'], 'Titouan')
        self.assertEqual(buckets[47].info['Id_Varieda'], '6')
        self.assertEqual(buckets[47].info['Indice_Fen'], '4.9')

    def test_run_no_id(self):
        importer = Importer()

        with self.assertRaises(MissingFieldError) as error_context:
            importer.run('ui/testsresources/test_no_id.zip',
                         Field.objects.get(pk=4),
                         BorderGeom)

        self.assertEqual(error_context.exception.field, 'Id')
        self.assertEqual(str(error_context.exception),
                         'Importer Error in file "ui/testsresources/test_no_id.zip": '
                         'Geometry file must contain a field Id '
                         'for each geometry.')

    def test_run_no_file(self):
        importer = Importer()

        with self.assertRaises(ImporterUnkownError) as error_context:
            importer.run('ui/testsresources/test_no_file.zip',
                         Field.objects.get(pk=4), BorderGeom)

        self.assertEqual(str(error_context.exception),
                         'Importer error in file "ui/testsresources/test_no_file.zip": '
                         'No such file or directory.')

    def test_run_wrong_file_extension(self):
        importer = Importer()

        with self.assertRaises(BadFormatError) as error_context:
            importer.run('ui/testsresources/test_no_shape.kml',
                         Field.objects.get(pk=4), BorderGeom)

        self.assertEqual(str(error_context.exception),
                         'Importer Error in file "ui/testsresources/test_no_shape.kml": '
                         'Bad File Format - Only zip file containing '
                         'Esri Shapefile assets are supported.')

    # def test_run_no_shape(self):
    #     importer = Importer()

    #     with self.assertRaises(ImporterError) as error_context:
    #         importer.run('ui/testsresources/test_no_shape.zip', Field.objects.get(pk=4), BorderGeom)

    #     self.assertEqual(str(error_context.exception.field), 'shape')
