from datetime import date

from django.test import TestCase
from django.core.urlresolvers import reverse
from django.test.client import Client

from ui.models import *
from ui.business.managers import *


class ExportDataTests(TestCase):

    fixtures = ['test_data.json']

    def setUp(self):
        self.client = Client()
        self.client.post(
            '/login/', {'username': 'testadmin', 'password': 'test'})

    def test_customer_field_bucket_data_export(self):
        response = self.client.get(reverse(
            'field_bucket_tabledata_csv',
            kwargs={'client_id': 3, 'field_id': 4}))

        self.assertEquals(response.status_code, 200)


        lines = response.content.split('\n')
        self.assertEquals(len(list(lines)), 50)
        self.assertEquals(
            lines[0], 'NDVI_STD,Variedad,Id_Varieda,Indice_Fen,'
            'NDVI_medio,Repeticion\r')
        self.assertIn(
            '0.111611570004,Sy 411-292,12,4.5,0.752626692613,4\r', lines)
        self.assertIn(
            '0.114666920966,Scrabble,1,4.5,0.776527911947,4\r', lines)


class ProductiveUnitView(TestCase):
    fixtures = ['test_data.json']

    def setUp(self):
        self.client = Client()
        self.client.post(
            '/login/', {'username': 'testadmin', 'password': 'test'})

    def test_noField(self):
        response = self.client.get(reverse(
            'field',
            kwargs={'client_id': 3, 'field_id': 11}))

        self.assertEquals(response.status_code, 404)

    def test_noEnvData(self):
        response = self.client.get(reverse(
            'field',
            kwargs={'client_id': 3, 'field_id': 9}))

        self.assertEquals(response.status_code, 200)

    def test_date_data_ok(self):
        response = self.client.get(reverse(
            'field',
            kwargs={'client_id': 3, 'field_id': 10}))

        self.assertEquals(response.status_code, 200)
        self.assertContains(response, 'filter_images_dates')
        self.assertListEqual(
            response.context['filter_images_dates'],
            ['2014/12/01','2014/12/06'])

    def test_date_data_no_image(self):
        response = self.client.get(reverse(
            'field',
            kwargs={'client_id': 2, 'field_id': 1}))

        self.assertEquals(response.status_code, 200)
        self.assertContains(response, 'filter_images_dates')
        self.assertListEqual(
            response.context['filter_images_dates'], [])


class KmlViews(TestCase):

    fixtures = ['test_data.json']

    def setUp(self):
        self.client = Client()
        self.client.post(
            '/login/', {'username': 'testadmin', 'password': 'test'})

    def test_EnvkmlFilterSimple(self):
        response = self.client.get(reverse(
            'fieldenvkml',
            kwargs={'client_id': 3, 'field_id': 4}) +
            '?info__gte__NDVI_STD=0.07&info__lte__NDVI_STD=0.12')

        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "NDVI")
        self.assertContains(response, "Polygon")

    def test_EnvkmlFilterAll(self):
        response = self.client.get(reverse(
            'fieldenvkml',
            kwargs={'client_id': 3, 'field_id': 4}) +
            '?info__gte__Repeticion=1&'
            'info__lte__Repeticion=4&'
            'info__gte__NDVI_STD=0.08&info__lte__NDVI_STD=0.13&'
            'info__gte__NDVI_medio=0.7&info__lte__NDVI_medio=0.82&'
            'info__gte__Indice_Fen=4.1&info__lte__Indice_Fen=4.9&'
            'info__gte__Id_Varieda=1&info__lte__Id_Varieda=12')

        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "NDVI")
        self.assertContains(response, "Polygon")
        self.assertContains(response, "Placemark", count=96)

    def test_EnvkmlFilterNumberComparison(self):
        response = self.client.get(reverse(
            'fieldenvkml',
            kwargs={'client_id': 3, 'field_id': 4}) +
            '?info__gte__Repeticion=1&'
            'info__lte__Repeticion=4&info__gte__NDVI_STD=0.07&'
            'info__lte__NDVI_STD=0.12&info__gte__Id_Varieda=15&'
            'info__lte__Id_Varieda=25')

        self.assertEquals(response.status_code, 200)
        self.assertNotContains(response, "NDVI")
        self.assertNotContains(response, "Polygon")
