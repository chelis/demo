from django.conf.urls import patterns, url
# from organizations.backends import invitation_backend
from .views import views

urlpatterns = patterns('',

    url(r'^(?P<client_id>\d+)/fields/$', views.customer_fields, name='fields'),
    url(r'^(?P<client_id>\d+)/fields/(?P<field_id>\d+)$', views.customer_field, name='field'),
    url(r'^(?P<client_id>\d+)/fields/(?P<field_id>\d+)/bucket/tabledata/$', views.customer_field_bucket_tabledata, name='field_bucket_tabledata'),
    url(r'^(?P<client_id>\d+)/fields/(?P<field_id>\d+)/border/tabledata/$', views.customer_field_border_tabledata, name='field_border_tabledata'),
    url(r'^(?P<client_id>\d+)/fields/(?P<field_id>\d+)/ol/$', views.customer_field_ol, name='fieldol'),
    url(r'^(?P<client_id>\d+)/fields/(?P<field_id>\d+)/env/kml/$', views.customer_field_env_kml, name='fieldenvkml'),
    url(r'^(?P<client_id>\d+)/fields/(?P<field_id>\d+)/border/kml/$', views.customer_field_border_kml, name='fieldborderkml'),
    url(r'^(?P<client_id>\d+)/fields/(?P<field_id>\d+)/bucket/tabledata/csv$', views.customer_field_bucket_data_export, name='field_bucket_tabledata_csv'),
    url(r'^(?P<client_id>\d+)/fields/(?P<field_id>\d+)/border/tabledata/csv$', views.customer_field_border_data_export, name='field_border_tabledata_csv'),

)

