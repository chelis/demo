from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import Http404
from django.contrib.gis.shortcuts import render_to_kml
from django.contrib import messages

from ..models import EnvironmentGeom, BorderGeom, Field, Customer

# TODO convert validation logic to decorator
@login_required
def customer_fields(request, client_id):
    try:
        if not client_id:
            raise Http404
        customer = Customer.objects.get(pk=client_id)

        if str(request.user.organizationuser.customer.id) != client_id:
            if not request.user.groups.filter(name="Admin").exists():
                raise Http404

    except Field.DoesNotExist:
        raise Http404
    return render(request, 'ui/field_list.html', {'customer': customer,
                                                  'field_list':
                                                  customer.field_set.all()})


@login_required
def customer_field(request, client_id, field_id):
    try:
        field = Field.objects.get(pk=field_id)
    except Field.DoesNotExist:
        raise Http404

    return render(request, 'ui/field.html', {
        'field': field,
        'filter_data_env': EnvironmentGeom.getMetadata(field),
        'filter_data_border': BorderGeom.getMetadata(field),
        'geometry_center': list(field.get_geom_center()),
        'filter_images_dates': [d.strftime('%Y/%m/%d') for d in field.productiveunitimage_set.
                                    values_list('date_taken', flat=True).
                                    order_by('date_taken').distinct()]
    })


@login_required
def customer_field_border_tabledata(request, client_id, field_id):
    try:
        field_data = EnvironmentGeom.objects.filter(
            productive_unit_id=field_id)
        from django.core import serializers
        data = serializers.serialize("python", field_data)

    except Field.DoesNotExist:
        raise Http404

    return render(request, 'ui/field_datasheet.html',  {
        'columns': BorderGeom.getMetadataColumns(field_id),
        'field_data': data,
    })


@login_required
def customer_field_bucket_tabledata(request, client_id, field_id):
    try:
        field_data = EnvironmentGeom.objects.filter(
            productive_unit_id=field_id)
        from django.core import serializers
        data = serializers.serialize("python", field_data)

    except Field.DoesNotExist:
        raise Http404

    return render(request, 'ui/field_datasheet.html',
                  {'columns': EnvironmentGeom.getMetadataColumns(field_id),
                   'field_data': data,
                   })


@login_required
def customer_field_ol(request, client_id, field_id):
    try:
        field = Field.objects.get(pk=field_id)
    except Field.DoesNotExist:
        raise Http404

    return render(request, 'ui/ol.html', {
        'field': field,
        'filter_data_env': EnvironmentGeom.getMetadata(field),
        'filter_data_border': BorderGeom.getMetadata(field)
    })


#@login_required removed to be able to query the server from localhost - FIXME
def customer_field_env_kml(request, client_id, field_id):
    try:
        filters = request.GET
        field = Field.objects.get(pk=field_id)
        return render_to_kml("gis/kml/placemarks_custom.kml",
                             {'places': EnvironmentGeom.objects.filter(
                                 productive_unit=field,
                                 **parseKmlFilterParams(filters)).kml,
                               'line_color': 'ffff0000'})

    except Field.DoesNotExist:
        raise Http404


#@login_required removed to be able to query the server from localhost - FIXME
def customer_field_border_kml(request, client_id, field_id):
    try:
        filters = request.GET
        field = Field.objects.get(pk=field_id)
        return render_to_kml("gis/kml/placemarks_custom.kml",
                             {'places': BorderGeom.objects.filter(
                                 productive_unit=field,
                                 **parseKmlFilterParams(filters)).kml,
                             'line_color': 'DD083300'})
    except Field.DoesNotExist:
        raise Http404


import csv
from django.http import HttpResponse


def customer_field_bucket_data_export(request, client_id, field_id):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')

    try:
        field_data = EnvironmentGeom.objects.filter(productive_unit=field_id)
        if len(field_data) == 0:
            raise Http404

        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(
            field_data[0].productive_unit.name)

        writer = csv.writer(response)

        columns = EnvironmentGeom.getMetadataColumns(field_id)

        writer.writerow(columns)
        for geom in field_data:
            writer.writerow([geom.info[c] for c in columns])

    except Field.DoesNotExist:
        raise Http404

    return response


def customer_field_border_data_export(request, client_id, field_id):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')

    try:
        field_data = EnvironmentGeom.objects.filter(productive_unit=field_id)
        if len(field_data) == 0:
            raise Http404

        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(
            field_data[0].productive_unit.name)

        writer = csv.writer(response)

        columns = EnvironmentGeom.getMetadataColumns(field_id)

        writer.writerow(columns)
        for geom in field_data:
            writer.writerow([geom.info[c] for c in columns])

    except Field.DoesNotExist:
        raise Http404

    return response


def parseKmlFilterParams(params_encoded):
    d = {}
    if params_encoded is None:
        return d
    for k, v in params_encoded.lists():
        if k != 'gm':
            value = v
            if len(v) == 1:
                value = v[0]

            if k.find("info__") == 0:
                nd = d.get(k[0:k.find("__", 6)], {})
                nd.update({k[k.find("__", 6) + 2:]: value})
                d[k[0:k.find("__", 6)]] = nd

            else:
                d[k] = v
                
    return d
