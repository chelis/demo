from django.core.exceptions import PermissionDenied
from functools import wraps


def belong_to_customer_required(client_id=None):
    """
    Decorator for views that checks whether a user belongs to the customer he is trying
    to acces, redirecting to the log-in page if necessary.
    If the raise_exception parameter is given the PermissionDenied exception
    is raised.
    """
    def wrapped(client_id=None):
        # First check if the user has the permission (even anon users)
        if client_id:
            if user.organizationuser.customer.id == client_id:
                return func(request, *args, **kwargs)
            # In case the 403 handler should be called raise the exception
            # if raise_exception:
            #     raise PermissionDenied
            # # As the last resort, show the login form
            #     return HttpResponseRedirect(reverse('login'))

    return wraps(func)(wrapped)



