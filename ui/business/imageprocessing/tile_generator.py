import tempfile
import shutil
import subprocess
import os
import errno
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
import logging

logger = logging.getLogger(__name__)


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


class TileGeneratorError(Exception):
    """ Base class for all tile generation exceptions """

    def __init__(self, file):
        self.file = file

    class Meta:
        abstract = True


class TileGenerator(object):

    def run(self, pu_image):
        """Creates a temp dir and calls a shell script to run gdal
        commands to reproject images, generate tiles, removed
        blank tiles and move result to the media folder.
        Media folder path is built out of customer, field, date_taken
        and image type values. If an error occurrs the processed is
        stopped and the error returned as a message which can be
        logged and displayed to the user in some way."""

        # TODO: LOCALE_PATHS = ()
        path = '{}/{}tiles/{}/'.format(settings.MEDIA_ROOT,
                                       pu_image.getAbsoluteBaseFolder(),
                                       pu_image.getImageTypeFolder())

        # TODO: hardcoding. API. Ensure temp file is removed
        temp_dir = tempfile.mkdtemp()
        # TODO: Pythonize
        try:
            mkdir_p(path)
            with open(settings.LOG_ROOT + '/tile_generator.log', 'wb') as f:
                # TODO: hardcoding. API
                ret_code = subprocess.call(
                    ['./ui/business/imageprocessing/tile_generator.sh',
                        pu_image.image.path,
                        temp_dir, path],
                    stdout=f,
                    stderr=subprocess.STDOUT,
                    shell=False)

                if ret_code != 0:
                    msg = _('An unknown error occurred and the image '
                            'could not be tiled.')

                    logger.error(
                        "TILE-GENERATOR|END|ERROR|file {}| {}".format(
                        pu_image.image.path,msg))

                    return [msg]
        except Exception as e:
            msg = _('An unknown error occurred and the image could not '
                    'be processed: {}'.format(e))

            logger.error(
                "TILE-GENERATOR|END|ERROR|file {}| {}".format(pu_image.image.path, msg))

            return [msg]
        finally:
            shutil.rmtree(temp_dir)

        return []
