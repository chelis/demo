#!/bin/sh
# 1 - input file path
# 2 - temp dir
# 3 - output dir

FILE_NAME='/'${1##*/}
TILES_DIR='/tiles/'
gdalwarp -t_srs "EPSG:3857" -dstnodata 0 $1 $2$FILE_NAME
gdal2tiles.py -z'15-23' --srcnodata 0 -v -n -g 'AIzaSyA3eC-QHrw6JQfrG0mc6nInA0hPPbfh9To'  $2$FILE_NAME $2$TILES_DIR

for OUTFILE in `find $2$TILES_DIR -name *.png` ; do
    # check if image is empty
    CNT=`gdalinfo -mm -noct -nomd "$OUTFILE" | grep 'Min/Max' | cut -f2 -d= | head -n 3 | tr ',' '\n' | uniq | wc -l`
    if [ "$CNT" -eq 1 ] ; then
       echo "Skipping blank image ...$OUTFILE"
       rm "$OUTFILE"
       continue
    fi
done

echo $2$TILES_DIR'*'
echo $3
eval cp -r $2$TILES_DIR'*' $3

