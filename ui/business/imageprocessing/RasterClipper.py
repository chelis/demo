import operator

from osgeo import gdal, gdalnumeric, ogr, osr
import Image, ImageDraw

# Raster image to clip
raster = "new12_geo.tif"

# Polygon shapefile used to clip
shp = "grid"

# Name of clip raster file(s)
output = "ndvigrid"

# This function will convert the rasterized clipper shapefile to a
# mask for use within GDAL.    
def imageToArray(i):
    """
    Converts a Python Imaging Library array to a gdalnumeric image.
    """
    a=gdalnumeric.fromstring(i.tostring(),'b')
    a.shape=i.im.size[1], i.im.size[0]
    return a

def arrayToImage(a):
    """
    Converts a gdalnumeric array to a Python Imaging Library Image.
    """
    i=Image.fromstring('L',(a.shape[1],a.shape[0]),
            (a.astype('b')).tostring())
    return i
     
def world2Pixel(geoMatrix, x, y):
  """
  Uses a gdal geomatrix (gdal.GetGeoTransform()) to calculate
  the pixel location of a geospatial coordinate 
  """
  ulX = geoMatrix[0]
  ulY = geoMatrix[3]
  xDist = geoMatrix[1]
  yDist = geoMatrix[5]
  rtnX = geoMatrix[2]
  rtnY = geoMatrix[4]
  pixel = int((x - ulX) / xDist)
  line = int((ulY - y) / xDist)
  return (pixel, line) 

def histogram(a, bins=range(0,256)):
  """
  Histogram function for multi-dimensional array.
  a = array
  bins = range of numbers to match 
  """
  fa = a.flat
  n = gdalnumeric.searchsorted(gdalnumeric.sort(fa), bins)
  n = gdalnumeric.concatenate([n, [len(fa)]])
  hist = n[1:]-n[:-1] 
  return hist

def stretch(a):
  """
  Performs a histogram stretch on a gdalnumeric array image.
  """
  hist = histogram(a)
  im = arrayToImage(a)   
  lut = []
  for b in range(0, len(hist), 256):
    # step size
    step = reduce(operator.add, hist[b:b+256]) / 255
    # create equalization lookup table
    n = 0
    for i in range(256):
      lut.append(n / step)
      n = n + hist[i+b]
  im = im.point(lut)
  return imageToArray(im)

# Load the source data as a gdalnumeric array
srcArray = gdalnumeric.LoadFile(raster)

# Also load as a gdal image to get geotransform (world file) info
srcImage = gdal.Open(raster)
geoTrans = srcImage.GetGeoTransform()

# Create an OGR layer from a Field boundary shapefile
field = ogr.Open("%s.shp" % shp)
lyr = field.GetLayer(shp)


# This script could be modified to do that. Instead of "poly = lyr.GetNextFeature()", you would build and array of polygons for each feature in the shapefile.

# And then you would need to put the "Map Points to Pixels" section in a loop to apply each polygon to the clipping mask. 

# I haven't tried this trick but it should work.

# When I have more time I can try to create an example. If you get it working let me know.


polygons = []
print lyr.GetFeatureCount() 
for i in range(0,lyr.GetFeatureCount() ):
  polygons.append(lyr.GetNextFeature())


for i,poly in enumerate(polygons):
  print poly.GetGeometryRef
  # Convert the layer extent to image pixel coordinates
  minX, maxX, minY, maxY = lyr.GetExtent()
  print minX, maxX, minY, maxY
  ulX, ulY = world2Pixel(geoTrans, minX, maxY)
  lrX, lrY = world2Pixel(geoTrans, maxX, minY)
  print ulX, ulY, lrX, lrY
  print geoTrans

  # Calculate the pixel size of the new image
  pxWidth = int(lrX - ulX)
  pxHeight = int(lrY - ulY)

  clip = srcArray[:, ulY:lrY, ulX:lrX]
  print size(clip)

  # Create a new geomatrix for the image
  geoTrans = list(geoTrans)
  geoTrans[0] = minX
  geoTrans[3] = maxY

  # Map points to pixels for drawing the field boundary on a blank
  # 8-bit, black and white, mask image.
  points = []
  pixels = []
  pts = poly.GetGeometryRef()


  for p in range(pts.GetPointCount()):
    points.append((pts.GetX(p), pts.GetY(p)))
  for p in points:
    pixels.append(world2Pixel(geoTrans, p[0], p[1]))
  rasterPoly = Image.new("L", (pxWidth, pxHeight), 1)
  rasterize = ImageDraw.Draw(rasterPoly)
  rasterize.polygon(pixels, 0)
  mask = imageToArray(rasterPoly)   

  # Clip the image using the mask
  clip = gdalnumeric.choose(mask, (clip, 0)).astype(gdalnumeric.uint8)

  # This image has 3 bands so we stretch each one to make them
  # visually brighter
  # for i in range(3):
  #   clip[i,:,:] = stretch(clip[i,:,:])

  # Save ndvi as tiff
  gdalnumeric.SaveArray(clip, "%s%d.tif" % (output,i) , format="GTiff", prototype=raster)

  # Save ndvi as an 8-bit jpeg for an easy, quick preview
  clip = clip.astype(gdalnumeric.uint8)
  gdalnumeric.SaveArray(clip, "%s%d.jpg" % (output,i), format="JPEG")



