import os
import itertools
from zipfile import ZipFile
import shutil
import tempfile
import logging

from django.utils.translation import ugettext_lazy as _

from django.contrib.gis.gdal import DataSource, SpatialReference

logger = logging.getLogger(__name__)


class ImporterError(Exception):

    """ Base class for all importer exceptions """

    def __init__(self, file):
        self.file = file

    class Meta:
        abstract = True


class BadFormatError(ImporterError):

    """Exception that indicates a bad file format."""

    def __init__(self, msg, file):
        super(BadFormatError, self).__init__(file)
        self.msg = msg

    def __str__(self):
        return _('Importer Error in file "%(filename)s": Bad File Format - '
                 '%(msg)s.') % {'filename': self.file, 'msg': self.msg}


class MissingFieldError(ImporterError):

    """Exception that indicates a missing mandatory field."""

    def __init__(self, field, file):
        super(MissingFieldError, self).__init__(file)
        self.field = field

    def __str__(self):
        return _('Importer Error in file "%(filename)s": Geometry file must '
                 'contain a field %(field)s for each geometry.') % \
            {'filename': self.file, 'field': self.field}


class ImporterUnkownError(ImporterError):

    """Exception that indicates something went wrong while importing."""

    def __init__(self, error, file):
        super(ImporterUnkownError, self).__init__(file)
        self.error = error

    def __str__(self):
        return _('Importer error in file "%(filename)s": %(msg)s.') %\
            {'filename': self.file, 'msg': self.error.strerror}


class Importer(object):

    def _extract_zip_file(self,  path):
        z = ZipFile(path)
        td = tempfile.mkdtemp()
        z.extractall(path=td)
        return td

    def _clean_up(self, path):
        shutil.rmtree(path)

    def __init__(self):
        self.oft_types_map = {'OFTInteger': 'NUMBER',
                              'OFTString': 'STRING',
                              'OFTReal': 'NUMBER',
                              'OFTDateTime': 'DATE'}

    def _getFilePath(self, filepath):
        retval = filepath
        if filepath.endswith("zip"):
            try:
                dirpath = self._extract_zip_file(filepath)
                files = os.listdir(dirpath)
                shp = [f for f in files if f.endswith('shp')][0]
                retval = dirpath + "/" + shp
                return retval
            except Exception as e:
                ex = ImporterUnkownError(e, filepath)
                logger.error(
                    "IMPORT|END|ERROR|file {}| {}".format(filepath, ex))

                raise ex
        else:
            ex = BadFormatError(_('Only zip file containing Esri Shapefile '
                                  'assets are supported'), filepath)

            logger.warning(
                'IMPORT|END|ERROR|file {}| {}'.format(filepath, ex))

            raise ex

    def _remove_old_geometries(self, klass, field):
        klass.objects.filter(productive_unit=field).delete()

    def _get_layer_to_import(self, filepath):
        dataSource = DataSource(self._getFilePath(filepath))
        layer = dataSource[0]
        return layer

    def _validate_fields_present(self, layer, filepath):
        if not 'id' in [x.lower() for x in layer.fields]:
            ex = MissingFieldError('Id', filepath)
            logger.warning(
                'IMPORT|END|ERROR|file {}| {}'.format(
                    filepath, ex))

            raise ex

        if layer.num_feat == 0:
            ex = MissingFieldError('feature', filepath)
            logger.warning(ex)
            raise ex

    def _get_geoms_in_4326(self, layer):
        return [geom.transform(SpatialReference('EPSG:4326'), clone=True)
                for geom in layer.get_geoms()]

    def run(self, filepath, field, klass, transform=False, verbose=True):
        """ Opens the zip with all shapefile assets, looks for a field name
        Id and throws exception if there is no such field. Transforms
        geometries to 4326 and creates an object of a subclass of BaseGeom,
        with fields  (number, geom, info), where number is the id, geom is the
        geometry field and info is a map of key:value where key can be a field
        name or a field type descriptor (fieldname_dt) and the values are
        either the value for that field for the geomtetry, or the type name in
        case the key ends with _dt
        """
        logger.info(
            'IMPORT|START|file {} to class {} and field {}'.format(
                filepath, klass, field))

        # clean up old data
        self._remove_old_geometries(klass, field)

        layer = self._get_layer_to_import(filepath)
        self._validate_fields_present(layer, filepath)

        geoms = self._get_geoms_in_4326(layer)
        field_names = layer.fields
        field_values = [layer.get_fields(fld) for fld in field_names]

        field_types = dict(zip([self.remove_trailing_underscores(fld) + '_dt'
                                for fld in field_names],
                               [self.oft_types_map[field_type.__name__]
                                for field_type in layer.field_types]))

        try:
            del field_types['Id_dt']
        except:
            pass

        try:
            del field_types['id_dt']
        except:
            pass

        for allfields_values in itertools.izip(geoms, *field_values):
            extrafields_dic = {}
            # geometry is not a field, get the remainder
            for name, value in zip(field_names, allfields_values[1:]):
                if name.lower() == 'id':
                    id_geom = value
                if name.lower() != 'id' and name.lower() != 'id_dt':
                    extrafields_dic.update(
                        {self.remove_trailing_underscores(name):  value})

            extrafields_dic.update(field_types)

            record = klass(number=id_geom, geom=allfields_values[0].wkt,
                           info=extrafields_dic, productive_unit=field)

            record.save()

            logger.info(
                'IMPORT|END|SUCCESS|file {} to class {}and field {}'.format(
                    filepath, klass, field))

    def remove_trailing_underscores(self, name):
        if name[-1:] == '_':
            return name[0:-1]

        return name
