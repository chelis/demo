from django.contrib.gis.db import models
from basic import Field
from django_hstore import hstore
import json
import logging

from ..business.managers import Importer, ImporterError

logger = logging.getLogger(__name__)


class BaseGeom(object):

    @staticmethod
    def asType(key, asTypes):
        t = asTypes.get(key)
        if t == 'NUMBER':
            return float
        else:
            return str

    @classmethod
    def getMetadata(klass, field):
        """Returns a JSON with two objects,
                columns_meta contains a dictionary of name, value where name is the field
                        name and  value the field type.
                columns returns a dictionary where the key is the field name and the value
                        is the list of values for that field across all geometries.
        """
        # types_map = {'IntegerField': 'NUMBER', 'CharField': 'STRING'}
        extrafields = klass.getMetadataColumns(field)
        extrafields_query = {k.lower(): "info -> '%s'" % k for k in extrafields}

        geoms = klass.objects.extra(select=extrafields_query,
                                    where=['productive_unit_id=%s'], params=[field.id])

        if geoms:
            a_geom = geoms[0]
            columns_meta = [{"name": key, "type": a_geom.info.get(key + '_dt', 'STRING')}
                            for key in extrafields]

            field_type_map = {key.lower(): a_geom.info.get(key + '_dt', 'STRING')
                              for key in extrafields}

            keys = field_type_map.keys()
            geoms_asdict = geoms.values(*keys)

            columns_data = {k: sorted(list({geom[k] for geom in geoms_asdict}),
                            key=klass.asType(k, field_type_map)) for k in keys}
        else:
            columns_meta = []
            columns_data = {}

        response = {}
        response.update({"columns_meta": columns_meta})
        response.update({"columns": columns_data})
        return json.dumps(response)

    @classmethod
    def getMetadataColumns(klass, field):
        """Returns the name of the fields in the HSTORE info field, which do not end with
         _dt, as we only want the name of the fields and not the data types."""
        return [f for f in klass.objects.hkeys(productive_unit_id=field, attr='info')
                if not f.endswith('_dt')]

    @classmethod
    def process_files(klass, field):
        borders = field.bordersFile.path
        environments = field.environmentsFile.path
        exceptions = []
        if environments:
            try:
                importer = Importer()
                importer.run(environments, field, EnvironmentGeom)
            except ImporterError as e:
                logger.error('Error Processing File:' + str(e))
                exceptions.append(str(e))

        if borders:
            try:
                importer = Importer()
                importer.run(borders, field, BorderGeom)
            except ImporterError as e:
                logger.error('Error Processing File:' + str(e))
                exceptions.append(str(e))

        return exceptions

    class Meta:
        abstract = True


class EnvironmentGeom(BaseGeom, models.Model):
    productive_unit = models.ForeignKey(Field)
    number = models.IntegerField(null=True)
    geom = models.GeometryField(srid=4326)
    info = hstore.DictionaryField()

    objects = hstore.HStoreGeoManager()

    class Meta:
        db_table = 'ui_environments'
        app_label = 'ui'


class BorderGeom(BaseGeom, models.Model):
    productive_unit = models.ForeignKey(Field)
    number = models.IntegerField(null=True)
    geom = models.PolygonField(srid=4326)
    info = hstore.DictionaryField()

    objects = hstore.HStoreGeoManager()

    class Meta:
        db_table = 'ui_borders'
        app_label = 'ui'
