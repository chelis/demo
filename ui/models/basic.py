from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
import datetime
from pyproj import Geod


class Customer(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'ui_customer'
        app_label = 'ui'

    def __unicode__(self):
        return u'%s' % (self.name, )

    def get_absolute_url(self):
        return reverse('admindashboard:customer', args=[str(self.id)])

    def get_edit_url(self):
        return reverse('admindashboard:customerupdate', args=[str(self.id)])

    def getFolder(self):
        return self.name.lower().replace(" ", "")


class OrganizationUser(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    customer = models.ForeignKey(Customer, blank=False)

    def get_absolute_url(self):
        return reverse('admindashboard:user', args=[str(self.id)])

    def get_edit_url(self):
        return reverse('admindashboard:userupdate', args=[str(self.user.id)])

    class Meta:
        db_table = 'ui_organizationuser'
        app_label = 'ui'


class Field(models.Model):
    name = models.CharField(max_length=50)
    customer = models.ForeignKey(Customer, blank=False, null=False)
    bordersFile = models.FileField(upload_to='static/', null=True)
    environmentsFile = models.FileField(upload_to='static/', null=True)

    class Meta:
        db_table = 'ui_field'
        app_label = 'ui'

    def __unicode__(self):
        return u'%s' % (self.name,)

    def getFolder(self):
        temp = self.name.lower().replace(" ", "")
        temp = temp.replace("-", "")
        return temp

    def getLastFolder(self):
        """ temporary method for retrieving last image folder for display.
        Ideally we would display all of them """
        date_folder = self.getAbsoluteFolder() + "/"
        images = self.productiveunitimage_set.all().order_by('-date_taken')
        if len(images) > 0:
            date_folder = images[0].getAbsoluteBaseFolder()

        return date_folder

    def getFalseColorFolder(self):
        return self.getLastFolder() + "tiles/falsecolor"

    def getNDVIFolder(self):
        return self.getLastFolder() + "tiles/ndvi"

    def getNDVIClassifiedFolder(self):
        return self.getLastFolder() + "tiles/ndviclassified"

    def getTerrainFolder(self):
        return self.getLastFolder() + "tiles/terrain"

    def getAbsoluteFolder(self):
        return self.customer.getFolder() + "/" + self.getFolder()

    def get_absolute_url(self):
        return reverse('admindashboard:field', args=[str(self.id)])

    def get_geom_center(self):
        g = Geod(ellps='WGS84')
        geoms = self.environmentgeom_set.all()
        if geoms:
            extent = geoms.extent()
            az12, az21, dist = g.inv(
                extent[0], extent[1], extent[2], extent[3])
            return g.fwd(extent[0], extent[1], az12, dist / 2)[1::-1]
        return []


class ImageType(models.Model):
    name = models.CharField(
        max_length=20, blank=False, null=False, unique=True)
    description = models.CharField(max_length=100)
    display_name = models.CharField(max_length=20, unique=True)

    def __unicode__(self):
        return u'%s' % (self.name, )

    def get_absolute_url(self):
        return reverse('admindashboard:imagetype', args=[str(self.id)])

    def get_edit_url(self):
        return reverse('admindashboard:imagetypeupdate', args=[str(self.id)])


def getFolder(instance, filename):
    path = instance.getAbsoluteBaseFolder()
    path = path + "originals/"
    return path + filename


class ProductiveUnitImage(models.Model):
    productive_unit = models.ForeignKey(Field)
    date_taken = models.DateField(default=datetime.date.today)

    image_type = models.ForeignKey(ImageType, null=True)
    image = models.ImageField(upload_to=getFolder, max_length=200)

    def getImageTypeFolder(self):
        value = self.image_type.name
        return "/" + value.lower().replace(" ", "")

    def getAbsoluteBaseFolder(self):
        path = self.productive_unit.getAbsoluteFolder()
        if self.date_taken:
            path = path + "{:/%Y/%m/%d/}".format(self.date_taken)
        return path

    def getImageTypeDescription(self):
        return self.image_type.name


from django.db.models.signals import post_save
# from django.contrib import messages
from decorators import disable_for_loaddata


@disable_for_loaddata
def my_callback(sender, **kwargs):
    """ Calls tiles generation method as an async task with celery """

    instance = kwargs['instance']
    if instance:
        from ..tasks import generate_tiles, error_handler, generation_completed
        generate_tiles.apply_async((instance,),
                                   link_error=error_handler.s(instance),
                                   link=generation_completed.s(instance))


post_save.connect(my_callback, sender=ProductiveUnitImage, weak=False)
