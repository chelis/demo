from django.core.urlresolvers import reverse
from lettuce import step, world
from lettuce.django import django_url
from nose.tools import assert_in, assert_not_in
from ui.models import Field

from common import *

PAGES = {
    "fields list": "/ui/1/fields/"
}


@step(r'Given I am logged in with test user')
def given_I_am_logged_in_with_test_user(step):
        createuser('test', 'test', 'testclient')
        login('test', 'test')


@step(r'Given I am logged in with cremeruser')
def given_I_am_logged_in_with_cremeruser(step):
        createuser('cremeruser', 'cremerpass', 'Cremer')
        assignfield('Cremer 1', 'Cremer')
        login('cremeruser', 'cremerpass')


@step(r'access the "(.*)" url for client "(.*)"')
def access_the_url(step, name, client):
        client = Customer.objects.filter(name=client)[0]
        world.browser.visit(django_url(reverse('fields', args=[client.id])))


@step(r'I click on "(.*)" link')
def i_click_on_field_link(step, fieldname):
        field = Field.objects.filter(name=fieldname)[0]
        link = world.browser.find_by_id("field_" + str(field.id))
        link.click()


@step(r'see a text "(.*)"')
def see_a_text(step, text):
    assert_in(text, world.browser.html)


@step(r'do not see a text "(.*)"')
def do_not_see_a_text(step, text):
    assert_not_in(text, world.browser.html)
