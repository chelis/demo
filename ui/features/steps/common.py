from ui.models import Field, Customer, OrganizationUser
from django.contrib.auth.models import User
from lettuce import world
from lettuce.django import django_url
from django.core.urlresolvers import reverse


def createuser(username, password, client):
    user = User.objects.create_user(username, 'lennon@thebeatles.com', password)
    user.save()
    customer = Customer.objects.filter(name=client).first()
    if not customer:
        customer = Customer.objects.create(name=client)
        customer.save()
    organizationUser = OrganizationUser(user=user, customer=customer)
    organizationUser.save()


def login(user, password):
    world.browser.visit(django_url(reverse('login')))
    world.browser.fill("username", user)
    world.browser.fill("password", password	)
    button = world.browser.find_by_id("btnSubmit")
    button.click()


def assignfield(fieldname, clientname):
    client = Customer.objects.filter(name=clientname)[0]
    field = Field(name=fieldname,
                  customer=client,
                  environment='1LWZyTVeIyB0UDKKv7R3tnriEWfb74_EEYyvL3--x',
                  borders='142hNafUBtq6eeVTuCt6xE0gU7kv9l8TnWxr49MlG')
    field.save()
