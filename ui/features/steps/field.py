from django.core.urlresolvers import reverse
from lettuce import step, world
from lettuce.django import django_url
from nose.tools import assert_in, assert_not_in
from ui.models import Field

from common import *

PAGES = {
    "field": "/ui/1/fields/1"
}

@step(r'Given I am logged in with cremeruser')
def given_I_am_logged_in_with_cremeruser(step):
	createuser('cremeruser','cremerpass','Cremer')
	assignfield('Cremer 1', 'Cremer')
	login('cremeruser', 'cremerpass')


@step(r'access the "(.*)" field url for client "(.*)"')
def access_the_url(step, name, client):
	client = Customer.objects.filter(name=client)[0]
	field = Field.objects.filter(name=name)[0]
	world.browser.visit(django_url(reverse('field', args=[client.id,field.id])))


@step('I see a tab "(.*)"')
def i_see_a_tab(step,tabid):
	assert world.browser.is_element_present_by_id(tabid)

@step('I see a check "(.*)"')
def i_see_a_check(step,checkid):
	assert world.browser.is_element_present_by_id(checkid)

@step('I see a map "(.*)"')
def i_see_a_map(step,mapid):
	assert world.browser.is_element_present_by_id(mapid)

@step('I see the filters "(.*)" under "(.*)" with prefix "(.*)"')
def i_see_the_filters_under_with_prefix(step,filterids, div,prefix):
	filters = filterids.split(",")
	for filterid in filters:
		assert world.browser.is_element_present_by_xpath('//*[@id="%s"]//*[@id="%s"]' %(div,prefix+filterid.upper()))
		
@step('I see the filter options "(.*)" under "(.*)" with prefix "(.*)"')
def i_see_the_filter_options_under_with_prefix(step,filterids, div,prefix):
	filters = filterids.split(",")
	for filterid in filters:
		assert world.browser.is_element_present_by_xpath('//*[@id="%s"]//*[@id="%s"]' %(div,prefix+filterid))


@step(r'Then I see filter slider ranges from (\d+) to (\d+) under "(.*)" with id "(.*)"')
def then_i_see_filter_slider_ranges_from_min_to_max_under_div_with_id(step,min,max, div,id):
	assert world.browser.is_element_present_by_xpath('//*[@id="%s"]//*[@id="%s" and @data-slider-min="%s" and @data-slider-max="%s" ]' %(div,id,min,max))
