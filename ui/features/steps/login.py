from django.core.urlresolvers import reverse
from lettuce import step, world
from lettuce.django import django_url
from nose.tools import assert_in, assert_not_in
from django.contrib.auth.models import User
from ui.models import Customer,OrganizationUser

PAGES = {
    "fields list": "/ui/1/fields/"
}

@step(r'Given a (.*) username with password (.*) was created for client "(.*)"')
def given_a_user_was_created_for_client(step, username, password, client):
 	user = User.objects.create_user(username, 'lennon@thebeatles.com', password)
 	user.save()
 	customer = Customer.objects.create(name=client)
 	customer.save()
 	organizationUser = OrganizationUser(user=user,customer=customer)
 	organizationUser.save()
 	
 	assert len(User.objects.filter(username=user)) == 1



@step(r'When I log in with user u\'([^\']*)\' and password u\'([^\']*)\'')
def when_i_log_in_with_user(step, user, password):
	world.browser.visit(django_url(reverse('login')))	
	world.browser.fill("username", user)
	world.browser.fill("password", password	)
	button = world.browser.find_by_id("btnSubmit")
	button.click()


@step(r'see a text "(.*)"')
def see_a_text(step, text):
    assert_in(text, world.browser.html)

@step(r'do not see a text "(.*)"')
def do_not_see_a_text(step, text):
    assert_not_in(text, world.browser.html)


