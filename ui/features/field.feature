Feature: Field


Scenario: Field load
		Given I am logged in with cremeruser
        When I access the "Cremer 1" field url for client "Cremer"
        Then I see a text "Filtrar por Ambientes"
        And I see a text "Filtrar por Contornos"
        And I see a tab "maptab"
        And I see a tab "tabletab"
        And I see a check "chk_environments"
        And I see a check "chk_borders"
        And I see a check "chk_rasters"
        And I see a map "map-canvas"


Scenario: I see the rigth options to filter borders
		Given I am logged in with cremeruser
        When I access the "Cremer 1" field url for client "Cremer"
        Then I see the filters "Sep_postes,Variedad,Perimeter,Name,Nombre,Sep_planta,Sep_filas,Estructura,Description,Area" under "borders" with prefix "dt-"


Scenario: I see the rigth options to filter ESTRUCTURA
		Given I am logged in with cremeruser
        When I access the "Cremer 1" field url for client "Cremer"
        Then I see the filter options "Canopia libre,Canopia Libre,Medio Libre,Parral,Tekauhata,VSP" under "collapseESTRUCTURA" with prefix "bordersESTRUCTURA"


Scenario: I see the rigth options to filter SEP_POSTES
                Given I am logged in with cremeruser
        When I access the "Cremer 1" field url for client "Cremer"
        Then I see filter slider ranges from 0 to 16 under "collapseSEP_POSTES" with id "slider_bordersSEP_POSTES"
