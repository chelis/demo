Feature: Fields list


Scenario: Fields list without any field
		Given I am logged in with test user
        When I access the "fields list" url for client "testclient"
        Then I see a text "No hay campos asignados"

Scenario: Fields list with one field
		Given I am logged in with cremeruser
        When I access the "fields list" url for client "Cremer"
        Then I see a text "Cremer 1"
        And I do not see a text "No hay campos asignados"


Scenario: Click on Field on Field list
		Given I am logged in with cremeruser
        When I access the "fields list" url for client "Cremer"
        When I click on "Cremer 1" link
        Then I see a text "Filtrar"
