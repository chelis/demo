/*
	Require and initialise PhantomCSS module
	Paths are relative to CasperJs directory
*/
var phantomcss = require('./../../node_modules/phantomcss/phantomcss.js');
var resemble = require('./../../node_modules/resemblejs/resemble.js');
//var phantomcss = require('./../../node_modules/phantomcss/Resemblejs/resemble.js');
var delay = 1000;
var excludeElements = ".gm-style-cc";

phantomcss.init({
    libraryRoot: '/home/chelis/development/freelance/Eiwa/proy/eiwa/node_modules/phantomcss',
    screenshotRoot: './ui/phantomcss/resources/screenshots',

    /*
        If failedComparisonsRoot is not defined failure images can still be found alongside the original and new images
    */
    failedComparisonsRoot: './ui/phantomcss/resources/failures',
    comparisonResultRoot: './ui/phantomcss/resources/results',

    /*
        Don't add label to generated failure image
    */
    addLabelToFailedImage: false,

    /*
        Mismatch tolerance defaults to  0.05%. Increasing this value will decrease test coverage
    */
    mismatchTolerance: 0,

    /*
        Callbacks for your specific integration
    */
    // onFail: function(test){ console.log(test.filename, test.mismatch); },
    // onPass: function(test){ console.log(test.filename); },
    // onTimeout: function(test){ console.log(test.filename); },
    // onComplete: function(allTests, noOfFails, noOfErrors){
    //     allTests.forEach(function(test){
    //         if(test.fail){
    //             console.log(test.filename, test.mismatch);
    //         }
    //     });
    // },


    /*
        Output styles for image failure outputs genrated by Resemble.js
    */
    // outputSettings: {
    //     errorColor: {
    //         red: 0,
    //         green: 255,
    //         blue: 255
    //     },
    //     errorType: 'movement',
    //     transparency: 0.3
    // }
});
/*
    Turn off CSS transitions and jQuery animations
*/



/*
	The test scenario
*/
casper.start( 'http://localhost:8000/ui/1/fields/1' , function(){this.echo(this.getTitle());});

casper.then(function(){this.fill('form', {
        username: 'testuser',
        password:  'test'
    }, true); });

casper.then( function(){this.echo(this.getTitle());})
casper.viewport(1024, 768);



casper.then(function(){
    phantomcss.screenshot( '#map-canvas', delay, excludeElements, 'see initial map');
});

casper.then(function(){
	casper.click('#chk_borders');
});

// casper.then(function(){
// 	casper.wait(1000);
// });

casper.then(function(){
    phantomcss.screenshot( '#map-canvas', delay, excludeElements, 'see shapefile');
});


casper.then(function(){
	casper.click('#bordersVARIEDADBonarda');
});

// casper.then(function(){
// 	casper.wait(1000);
// });

casper.then(function(){
	phantomcss.screenshot( '#map-canvas', delay, excludeElements, 'see shapefile Bonarda');
});


casper.then(function(){
	casper.wait(1000);
});

casper.then(function(){
    casper.click('#bordersVARIEDADBonarda');
});

casper.then(function(){
    casper.wait(1000);
});

casper.then(function(){

	var elem = casper.evaluate(function(){
        var elem = $(document.querySelector('input[name="SEP_FILAS"]'))
		 elem.slider('setValue', [3.5,5], true);
         return elem.slider('getValue');

  	});
    console.log(elem)
});

casper.then(function(){
	casper.wait(1000);
});

casper.then(function(){
    phantomcss.screenshot( '#map-canvas', delay, excludeElements, 'see shapefile Bonarda plus sep filas');
});

casper.then( function now_check_the_screenshots(){
	phantomcss.compareAll();

});

casper.then( function end_it(){
	casper.test.done();
});

/*
Casper runs tests
*/
casper.run(function(){
	console.log('\nTHE END.');
	phantom.exit(phantomcss.getExitStatus());
});

