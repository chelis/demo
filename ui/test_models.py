import json

from django.test import TestCase

from ui.models import EnvironmentGeom, Field


class BaseGeomTestCase(TestCase):
    fixtures = ['test_data.json']

    def test_getMetadata_simple(self):
        field = Field.objects.get(pk=4)

        result_as_dict = json.loads(EnvironmentGeom.getMetadata(field))

        self.assertIn('columns_meta', result_as_dict)
        self.assertIn('columns', result_as_dict)
        self.assertEquals(len(result_as_dict['columns_meta']), 6)
        self.assertEquals(result_as_dict['columns_meta'], [{u'type': u'NUMBER', u'name': u'NDVI_STD'}, {u'type': u'STRING', u'name': u'Variedad'}, {u'type': u'NUMBER', u'name': u'Id_Varieda'}, {u'type': u'NUMBER', u'name': u'Indice_Fen'}, {u'type': u'NUMBER', u'name': u'NDVI_medio'}, {u'type': u'NUMBER', u'name': u'Repeticion'}] )
        self.assertEquals(len(result_as_dict['columns']), 6)
        self.assertEquals(len(result_as_dict['columns']['variedad']), 6)
        self.assertEquals(len(result_as_dict['columns']['id_varieda']), 12)


    def test_getMetadata_noGeom(self):
        field = Field.objects.get(pk=9)

        result_as_dict = json.loads(EnvironmentGeom.getMetadata(field))

        self.assertIn('columns_meta', result_as_dict)
        self.assertIn('columns', result_as_dict)
        self.assertEquals(len(result_as_dict['columns_meta']), 0)
        self.assertEquals(len(result_as_dict['columns']), 0)
