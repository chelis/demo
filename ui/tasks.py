from __future__ import absolute_import

from django.contrib.auth.models import User
from django.core.cache import cache

from celery import shared_task
from celery.result import AsyncResult
from celery.utils.log import get_task_logger
from .business.imageprocessing import TileGenerator
from notification import models as notification

tiles_generator = TileGenerator()
LOCK_EXPIRE = 60 * 5  # Lock expires in 5 minutes
logger = get_task_logger(__name__)


@shared_task
def generate_tiles(pu_image):
    # return tiles_generator.run(pu_image)
    lock_id = '{0}-lock-{1}'.format('generate_tiles',
                                    pu_image.productive_unit.id)

    # cache.add fails if if the key already exists
    acquire_lock = lambda: cache.add(lock_id, 'true', LOCK_EXPIRE)

    # memcache delete is very slow, but we have to use it to take
    # advantage of using add() for atomic locking
    release_lock = lambda: cache.delete(lock_id)

    logger.info('TASK|TILES|START|Image: %s', str(pu_image))
    if acquire_lock():
        try:
            result = tiles_generator.run(pu_image)
            logger.info('TASK|TILES|END|SUCCESS|Image: %s', str(pu_image))

        except Exception as e:
            logger.error(
                'TASK|TILES|END|ERROR|Image: %s|{}', str(pu_image), e)

        finally:
            release_lock()
        return result

    logger.warning(
        'TASK|TILES|WARNING|Image %s is already being imported by '
        'another worker. Retrying', str(pu_image))

    generate_tiles.retry(args=pu_image, countdown=LOCK_EXPIRE)


@shared_task
def error_handler(uuid, pu_image):
    result = AsyncResult(uuid)
    exc = result.get(propagate=False)

    logger.error('Task {0} raised exception: {1!r}\n{2!r}'.format(
        uuid, exc, result.traceback))

    notification.send([User.objects.get(username='mcampo')],
                      'image_processed',
                      {'image': pu_image,
                       'messages': 'Task {0} raised exception: {1!r}\n'
                       '{2!r}'.format(
                           uuid, exc, result.traceback)})


@shared_task
def generation_completed(result, pu_image):
    notification.send([User.objects.get(username='mcampo')],
                      'image_processed',
                      {'image': pu_image, 'messages': result})

    logger.info(
        'TASK|TILES-COMPLETED|NOTIFICATION-SENT', str(pu_image))
