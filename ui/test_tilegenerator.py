from django.test import TestCase, override_settings

from ui.models import Field, Customer, ProductiveUnitImage, ImageType
from ui.business.imageprocessing.tile_generator import *
from django.conf import settings
import os
from django.core.files import File
import shutil
import datetime


@override_settings(MEDIA_ROOT='/tmp/django_test/')
class TileGeneratorTests(TestCase):

    """Tests image tile generation"""

    def tearDown(self):
        shutil.rmtree('/tmp/django_test/test')

    def test_generate_tiles_simple(self):
        with open('ui/testsresources/Mosaico_Sony_chico.tif') as testimage:
            generator = TileGenerator()

            path = settings.MEDIA_ROOT + \
                "/test/testproductiveunit/2014/11/10/tiles/terrain"
            customer = Customer(name="test")
            customer.save()
            productiveunit = Field(
                customer=customer, name="testproductiveunit")
            productiveunit.save()
            it = ImageType.objects.get(name='Terrain')
            imagepu = ProductiveUnitImage(productive_unit=productiveunit,
                                          image=File(testimage),
                                          date_taken=datetime.date(
                                              2014, 11, 10),
                                          image_type=it)
            imagepu.save()

            res = generator.run(imagepu)
            self.assertEqual([], res)
            self.assertTrue(os.path.exists(path))

    def test_generate_tiles_error(self):
        with open('ui/testsresources/Mosaico_Sony_chico.tif') as testimage:
            generator = TileGenerator()

            customer = Customer(name="test")
            customer.save()
            it = ImageType.objects.get(name='Terrain')
            productiveunit = Field(
                customer=customer, name="testproductiveunit")
            productiveunit.save()
            imagepu = ProductiveUnitImage(productive_unit=productiveunit,
                                          image=File(testimage),
                                          date_taken=datetime.date(
                                              2014, 11, 10),
                                          image_type=it)
            # imagepu.save() don't save so we generate an error as the image
            # wont be there

            res = generator.run(imagepu)
            self.assertEqual(
                ['An unknown error occurred and the image could not '
                 'be tiled.'], res)
