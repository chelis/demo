(function(){

	function addFilters(filter_data,ul_element) {
		var columns = filter_data.columns_meta;
		columns = columns.sort( function(a,b) {
          			return (a.name.toUpperCase() < b.name.toUpperCase());
		});

		for (var i = 0; i < columns.length; i++) {
			var column = columns[i];
			var type = column.type;
			var columnName = column.name;

				storeFilterValuesForColumn(columnName, columnName, type, ul_element,true, filter_data.columns[columnName.toLowerCase()]);

		};

	}

	function storeFilterValuesForColumn( columnId, columnName, columnType, ul_element, addTitle,filterdata){
		var data = filterdata;
		//if(data[data.length] == "") return;
		var cList = $('#' + ul_element)
		var container = cList;
		if(addTitle){
		    var div1, div ;
			div1 = $('<div data-toggle="collapse"/>').attr('data-target', '#collapse' + columnName ).attr('id', 'dt-'+columnId.toUpperCase()).text(filterName(columnName));
			div = $('<div/>').addClass('collapse').attr('id', 'collapse' + columnName )
			cList.append(div1)
			div.appendTo(cList);
			container = div;
		}
		if(columnType == "NUMBER"){
			createSlider(columnId, columnName, columnType, data, ul_element).appendTo(container);
		}

		if(columnType == "STRING"){
			for(i = 0; i < data.length; i++) {
				 if(data[i] != ""){
				 	createCheckbox(columnId, columnName, columnType, data, ul_element).appendTo(container);
				 }
			}
		}
	}

	function createSlider(columnId, columnName, columnType, data, ul_element){
		// TODO precision & step
			var container = $('<div />').attr('id',"fl-"+columnName.toUpperCase());
			parts = data[0].split(".").length
			precision = 0
			if(parts > 1) {
				precision = data[0].split(".")[1].length
				if (precision > 3) {precision = 2}
			}
			mult = Math.pow(10,precision)
			min = Math.floor(data[0]*mult)/mult
			max = Math.ceil(data[data.length -1]*mult)/mult

 
			var slider = $('<input />', {
                                        "id":"slider_" + ul_element + columnName,
                                        "name":columnName,
                                        "data-slider-min": min,
                                        "data-slider-max":max,
                                        "data-slider-value": "["+ min + "," + max + "]",
                                        "data-slider-step": (max - min)/10,
                                        type: "text",
                                        "class":"span2 " + ul_element + 'slider',
                                        value: "",
                                        style:"width: 70%;",
                                        "data-slider-handle":"triangle",
                                       "data-slider-precision": precision


	                        }).on('slide', function(e) { updateQuery(e, "."+ul_element)})
	                        slider.appendTo(container)


			slider.bootstrapSlider({});
			return container
	}

	function createCheckbox(columnId,columnName,columnType, data,ul_element) {
		var container = $('<div/>',{'class':"checkbox"})
		$('<input />',{
				type:"checkbox",
				"class":ul_element + "updatemap",
				'id': ul_element + columnName + data[i],
				'name': columnName,
				"value": data[i],
				text:data[i],
				click: function(e) { updateQuery(e, "."+ul_element)}
			}).appendTo(container);
		$('<label/>', {'for': data[i],'html': data[i]}).appendTo(container);
		return container

	}

	function filterName(name){
	      var name = name.toLowerCase().replace(/_/g," ")
	      return name.charAt(0).toUpperCase() + name.slice(1,name.length)
	}

	function addImageFilter(data) {
		if (data == null || data.length == 0) { return }
		container = $('#image-date-filter')
		container.append($('<span class="label label-success">' + data[0] + '</span>    <span/>'))
		var slider = $('<input />', {
	                                "id":"slider-image-date",
	                                "name":"image-date",
	                                "data-slider-min": 1,
	                                "data-slider-max": data.length,
	                                "data-slider-value": 1,
	                                "data-slider-step": 1,
	                                "class":"image-date-slider",
	                                value: data.length,
	                                style:"width: " + data.length*10 + "%;",
	                                "data-slider-handle":"custom",
	                                "data-slider-id": "slider-image-date"
	                                
	                    }) .on('slideStop', function(e) { updateTiles() })
		slider.appendTo(container)
		container.append($('<span class="label label-success">' + data[data.length -1] + '</span>'))
		slider.bootstrapSlider({
			formatter: function(value){return data[value-1]},
			tooltip:'always'
		})
		var cList = $('#image-date-filter').append(slider)
		return slider

	}

	function updateTiles(folder) {
		for ( layerIdx in overlayMapTypes ){
			layer = overlayMapTypes[layerIdx]
			if ( layer_selected(layer) ) { 
				removeLayer(layer)
				addLayer(layer)
			}
		}
	}

	var map;
	var borders;
	var myParser;
	var layers;
	var mapMinZoom = 10;
	var mapMaxZoom = 23;
	var zoom = 17;
	var previous_zoom = zoom
	var date_slider = null

	function initialize() {
		layers = []

		center =new google.maps.LatLng(geometry_center[0], geometry_center[1]) 

		map = new google.maps.Map(document.getElementById('map-canvas'), {
			center: center,
			zoom: zoom,
			mapTypeControl: false,
			mapTypeId: google.maps.MapTypeId.SATELLITE,
			scaleControl: true,
			rotateControl: true,
			maxZoom: mapMaxZoom,
			zoomControlOptions: {
					style: google.maps.ZoomControlStyle.SMALL
				}

		});

		google.maps.event.addListener(map,'mousemove',function(event) {
		document.getElementById('latlong').innerHTML = event.latLng.lat() + ', ' + event.latLng.lng() });


		google.maps.event.addListener(map, 'zoom_changed', function() {
			 var zoom_level = map.getZoom();
			 var max_gm_zoom = 19
			 if (zoom_level == max_gm_zoom && previous_zoom < zoom_level) {
			 	map.setMapTypeId('ter');
			 }
			 if (zoom_level ==(max_gm_zoom - 1)  && previous_zoom > zoom_level) {
			 	map.setMapTypeId(google.maps.MapTypeId.SATELLITE);
			 }
			 previous_zoom = zoom_level

		  });

		createLayers();
		map.mapTypes.set('ter', layers['terrain']); // hardcoded
		addFilters(filter_data_border, 'borders')
		addFilters(filter_data_env,  'environments')
		date_slider = addImageFilter(filter_images_dates)


	}

	function layer_selected(layer) {
		return $('#chk_' + layer ).is(':checked')
	}

	
	function createLayers() {
		$('.chk_layer,[layer-type="raster"]').each( function() {
			createImageLayer($(this).data('layer'), $(this).data('layer')) })	

	 	createKMLLayer('environments','env', null)		
		createKMLLayer('borders', 'border', null)		
	 }



	function createKMLLayer(layer, folder,params) {
	 	if (params == null) {filters = ""} else {filters= params}
		
		// TODO: hardcoded as geoxml3 was giving some problems and google maps needs this publicly available	
		kml_url = window.location.href + '/' + folder + '/kml?gm=1&' + filters
	 	//kml_url = "http://app.eiwa.ag/ui/4/fields/5/env/kml?gm=6&" + filters
	 	var kml_layer = new google.maps.KmlLayer({
		    url: kml_url,
		    preserveViewport: true
		  });

	 	layers[layer] = kml_layer

	}

	function createImageLayer(key, folder) {
		//Cremer var mapBounds = new google.maps.LatLngBounds(new google.maps.LatLng(-26.0669180261, -65.9867602879), new google.maps.LatLng(-26.0395353489, -65.9551489653));
		//var mapBounds = new google.maps.LatLngBounds(new google.maps.LatLng(-34.55771388565057, -60.367219001054764), new google.maps.LatLng(-34.55817334541287, -60.3209562599659));
		// TODO: this has to be dynamic

		var imageMapTypeLayer = new google.maps.ImageMapType({
		    minZoom: mapMinZoom,			
		    maxZoom: mapMaxZoom,
		    name: key,	
		    getTileUrl: function(coord, zoom) {
		    	// var date_slider = $('#slider-image-date').bootstrapSlider()
		    	date = ""
		    	if (date_slider != null) {
		    		date = "/" + filter_images_dates[date_slider.bootstrapSlider('getValue') - 1]
		    	}
		    	if ((zoom < mapMinZoom) || (zoom > mapMaxZoom)) {
			       return "http://www.maptiler.org/img/none.png";
			    }
			    var ymax = 1 << zoom;
			    var y = ymax - coord.y -1;

			     var tileBounds = new google.maps.LatLngBounds(
			                  map.getProjection().fromPointToLatLng( new google.maps.Point( (coord.x), (coord.y) )  ),
			                  map.getProjection().fromPointToLatLng( new google.maps.Point( (coord.x), (coord.y)))
			              );


		      	if (tileBounds.intersects(tileBounds)) {
			                  return image_folder + date +  "/tiles/" + folder + "/" + zoom+"/"+coord.x+"/"+y+".png";

			              } else {
			                  return "http://www.maptiler.org/img/none.png";
			              }


		    },
		    tileSize: new google.maps.Size(256, 256)
		  });

		layers[key] = imageMapTypeLayer
		map.overlayMapTypes.push(null)

	}

	function updateQuery(e, class_selector) {
		var values = ""
		$(class_selector + 'updatemap:checked').each(function(index) {
			var id = $(this).attr('name');
			if(values.length > 0 ) {
				values += "&"
			}

			values += "info__contains__" +  id + "="+ encodeURIComponent($(this).attr('value') )});


		var sliders = updateQuerySliders(e, class_selector)
		var result = values +   sliders

		var layerType = class_selector.slice(1,class_selector.length)
		if(layerType == 'borders') {
			removeLayer('borders')
			createKMLLayer('borders', 'border', result)		
			addLayer('borders')
		} else {
			removeLayer('environments')
			createKMLLayer('environments', 'env', result)		
			addLayer('environments')


		}
	}

	function updateQuerySliders(e, class_selector) {
		var values = {}
		$( class_selector + 'slider').each(function(index) {
			var id=$(this).attr('name');
			if(values[id] == null) {
				values[id] = [];
			}
		    values[id].push( $(this).bootstrapSlider('getValue') )
		});

		var columnvalues = ""
		for (col in values) {
			columnvalues += "&info__gte__" + col + "="  + values[col][0][0]
			columnvalues += "&" + "info__lte__" + col + "="  + values[col][0][1]

		}
		return columnvalues

	};


	google.maps.event.addDomListener($('updatemap').get(),'click', updateQuery)

	$('.chk_layer').each( function(index, elem) { $(elem).click(function(event) { toggleLayer(event, elem);}) })


	function toggleLayer(event, control){

		if(control.checked){
			addLayer($(control).data('layer'));
		} else
		{
			removeLayer($(control).data('layer'))
		}

	}


	function removeLayer(layer) {
		if( typeof layers[layer].setMap === 'function') {
			layers[layer].setMap(null)
		} else {
			
			map.overlayMapTypes.setAt(overlayMapTypes.indexOf(layer), null);
		}
	 }



	 function addLayer(layer) {
	 	if( typeof layers[layer].setMap === 'function') {
			layers[layer].setMap(map)
		} else {
			
			map.overlayMapTypes.setAt(overlayMapTypes.indexOf(layer), layers[layer]);
			
		}
	 }



     function getTableData(field_id, tab) {
        var queryUrlHead =  window.location.href + "/" + tab + "/tabledata";
        
        var queryurl = encodeURI(queryUrlHead);

	var request = $.ajax({
	  url: queryurl,
	  type: "GET",
	  dataType: "html"
	});
	 
	request.done(function( msg ) {
	  $( "#table_container_" + tab ).html( msg );
	});
	 
	request.fail(function( jqXHR, textStatus ) {
	  $( "#table_container_" + tab ).html( "No data available" );
	});


    };


    google.maps.event.addDomListener(window, 'load', initialize);
    $(window).load(function() { getTableData(field_id,"bucket") });
    

 
    
})();
