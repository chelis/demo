# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields
import ui.models.geo
import django_hstore.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BorderGeom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.IntegerField(null=True)),
                ('geom', django.contrib.gis.db.models.fields.PolygonField(srid=4326)),
                ('info', django_hstore.fields.DictionaryField()),
            ],
            options={
                'db_table': 'ui_borders',
            },
            bases=(ui.models.geo.BaseGeom, models.Model),
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'ui_customer',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EnvironmentGeom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.IntegerField(null=True)),
                ('geom', django.contrib.gis.db.models.fields.PolygonField(srid=4326)),
                ('info', django_hstore.fields.DictionaryField()),
            ],
            options={
                'db_table': 'ui_environments',
            },
            bases=(ui.models.geo.BaseGeom, models.Model),
        ),
        migrations.CreateModel(
            name='Field',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('borders', models.CharField(max_length=50)),
                ('environment', models.CharField(max_length=50)),
                ('bordersFile', models.FileField(null=True, upload_to=b'static/')),
                ('environmentsFile', models.FileField(null=True, upload_to=b'static/')),
                ('customer', models.ForeignKey(to='ui.Customer')),
            ],
            options={
                'db_table': 'ui_field',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrganizationUser',
            fields=[
                ('user', models.OneToOneField(primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('customer', models.ForeignKey(to='ui.Customer')),
            ],
            options={
                'db_table': 'ui_organizationuser',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='environmentgeom',
            name='productive_unit',
            field=models.ForeignKey(to='ui.Field'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bordergeom',
            name='productive_unit',
            field=models.ForeignKey(to='ui.Field'),
            preserve_default=True,
        ),
    ]
