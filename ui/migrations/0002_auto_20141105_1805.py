# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ui', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductiveUnitImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_taken', models.DateField()),
                ('imageType', models.CharField(default=b'TR', max_length=2, choices=[(b'TR', b'Terrain'), (b'FC', b'False Colors'), (b'ND', b'NDVI'), (b'NM', b'NDVI mean')])),
                ('productive_unit', models.ForeignKey(to='ui.Field')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='field',
            name='borders',
        ),
        migrations.RemoveField(
            model_name='field',
            name='environment',
        ),
    ]
