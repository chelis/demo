# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ui.models.basic


class Migration(migrations.Migration):

    dependencies = [
        ('ui', '0003_auto_20141105_1946'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productiveunitimage',
            name='image',
            field=models.ImageField(max_length=200, upload_to=ui.models.basic.getFolder),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='productiveunitimage',
            name='image_type',
            field=models.CharField(default=b'TR', max_length=2, choices=[(b'TR', b'Terrain'), (b'FC', b'False Colors'), (b'ND', b'NDVI'), (b'NM', b'NDVI classified')]),
            preserve_default=True,
        ),
    ]
