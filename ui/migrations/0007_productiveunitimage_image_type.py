# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def load_data(apps, schema_editor):
    ImageType = apps.get_model("ui", "ImageType")
    ImageType(name="Terrain",
              description="Terrain",
              display_name="Terreno"
              ).save()

    ImageType(name="False Color",
              description="False Color",
              display_name="Colores Falsos"
              ).save()

    ImageType(name="NDVI",
              description="NDVI",
              display_name="NDVI"
              ).save()

    ImageType(name="NDVI Classified",
              description="NDVI medio por parcela "
              "(calculado según la Media estadística)",
              display_name="NDVI Medio"
              ).save()


def populate_fk(apps, schema_editor):

    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.

    TERRAIN = 'TR'
    FALSE_COLORS = 'FC'
    NDVI = 'ND'
    NDVI_MEAN = 'NM'

    IMAGE_TYPE_CHOICES = (
        (TERRAIN, 'Terrain'),
        (FALSE_COLORS, 'False Color'),
        (NDVI, 'NDVI'),
        (NDVI_MEAN, 'NDVI classified'),
    )
    Image = apps.get_model("ui", "ProductiveUnitImage")
    ImageType = apps.get_model("ui", "ImageType")
    for image in Image.objects.all():
        image_type_desc = dict(IMAGE_TYPE_CHOICES)[image.image_type_code]
        image.image_type = ImageType.objects.get(
            name=image_type_desc)
        image.save()

class Migration(migrations.Migration):

    dependencies = [
        ('ui', '0006_auto_20141216_1734'),
    ]

    operations = [
        migrations.AddField(
            model_name='productiveunitimage',
            name='image_type',
            field=models.ForeignKey(to='ui.ImageType', null=True),
            preserve_default=True,
        ),
        migrations.RunPython(load_data),
        migrations.RunPython(populate_fk),
    ]
