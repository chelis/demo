# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('ui', '0004_auto_20141114_2043'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productiveunitimage',
            name='date_taken',
            field=models.DateField(default=datetime.date.today),
            preserve_default=True,
        ),
    ]
