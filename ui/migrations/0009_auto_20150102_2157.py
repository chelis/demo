# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('ui', '0008_remove_productiveunitimage_image_type_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='environmentgeom',
            name='geom',
            field=django.contrib.gis.db.models.fields.GeometryField(srid=4326),
            preserve_default=True,
        ),
    ]
