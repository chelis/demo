# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ui', '0007_productiveunitimage_image_type'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productiveunitimage',
            name='image_type_code',
        ),
    ]
