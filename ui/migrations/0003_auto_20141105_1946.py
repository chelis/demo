# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ui', '0002_auto_20141105_1805'),
    ]

    operations = [
        migrations.RenameField(
            model_name='productiveunitimage',
            old_name='imageType',
            new_name='image_type',
        ),
        migrations.AddField(
            model_name='productiveunitimage',
            name='image',
            field=models.ImageField(default='', upload_to=b''),
            preserve_default=False,
        ),
    ]
