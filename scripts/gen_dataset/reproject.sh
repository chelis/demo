INPUT=Mosaico_Sony.tif
OUTPUT=processed/Mosaico_Sony.tif
gdalwarp -t_srs "EPSG:3857" -dstnodata 0 $INPUT $OUTPUT


INPUT=Mosaico_Tetra.tif
OUTPUT=processed/Mosaico_Tetra.tif
gdalwarp -t_srs "EPSG:3857" -dstnodata 0 $INPUT $OUTPUT

INPUT=NDVI_mean.tif
OUTPUT=processed/NDVI_mean.tif
gdalwarp -t_srs "EPSG:3857" -dstnodata 0 $INPUT $OUTPUT


INPUT=NDVI.tif
OUTPUT=processed/NDVI.tif
gdalwarp -t_srs "EPSG:3857" -dstnodata 0 $INPUT $OUTPUT
