FOLDER = './tiles'

for OUTFILE in `find $FOLDER -name *.png` ; do
    # check if image is empty
    CNT=`gdalinfo -mm -noct -nomd "$OUTFILE" | grep 'Min/Max' | cut -f2 -d= | head -n 3 | tr ',' '\n' | uniq | wc -l`
    if [ "$CNT" -eq 1 ] ; then
       echo "Skipping blank image ...$OUTFILE"
       \rm "$OUTFILE"
       continue
    fi
done
