from django.contrib.auth.forms import PasswordChangeForm


class LoginPasswordChangeForm(PasswordChangeForm):

    def save(self):
        self.user.organizationuser.changepassword = False
        self.user.organizationuser.save()
        super(PasswordChangeForm, self).save()
