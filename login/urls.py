from django.conf.urls import patterns, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
import views
from registration.backends.default.views import ActivationView
from django.contrib.auth.views import *


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^login/?next=/$', 'django.contrib.auth.views.login', {'template_name': 'login/login.html'}, name='login'),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'login/login.html'},name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'template_name': 'login/logout.html'}, name='logout'),
    url(r'^loginSuccess/$', views.loginSuccess, name='loginSuccess'),
    url(r'^activate/(?P<activation_key>[-_\w]+)$', ActivationView.as_view(), name='registration_activate'),
    url(r'^activate/success$', views.activation_complete, name='registration_activation_complete'),
    url(r'^profile/changepassword$', password_change, {'template_name':'registration/password_change.html', 'post_change_redirect':'auth:loginSuccess'},name='changepassword'),
    url(r'^profile/changepassword/success$', login, name='password_change_done'),


)
