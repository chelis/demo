from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.conf import settings

from braces.views import GroupRequiredMixin
from forms import *


class AdminRequiredMixin(GroupRequiredMixin):
    group_required = u"Admin"


@login_required
def activation_complete(request):
    context = {}
    return render(request, 'registration/activation_complete.html', context)


@login_required
def loginSuccess(request):
    """
    Redirects users based on whether they are in the admins group
    """
    if request.user.groups.filter(name="Admin").exists():

        # user is an admin
        return redirect(settings.ADMIN_LANDING_PAGE)
    else:
        customer = request.user.organizationuser.customer

        return redirect(reverse(settings.CUSTOMER_LANDING_PAGE, args=[customer.id]))


