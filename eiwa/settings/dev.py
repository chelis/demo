# STATIC_ROOT = "/home/chelis/development/freelance/Eiwa/proy/eiwa/staticfiles/"

# PIPELINE_ENABLED = True
# PIPELINE = True

# STATICFILES_FINDERS = (
#     'pipeline.finders.FileSystemFinder',
#     'pipeline.finders.AppDirectoriesFinder',
#     'pipeline.finders.PipelineFinder',
#     'pipeline.finders.CachedFileFinder',
# )
DEBUG = True

TEMPLATE_DEBUG = True


DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'eiwadb-dev',
        'USER': 'eiwadb-dev',
        'PASSWORD': 'eiwadb',
        'HOST': ''
    }
}

EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
