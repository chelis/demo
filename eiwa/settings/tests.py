# STATIC_ROOT = "/home/chelis/development/freelance/Eiwa/proy/eiwa/staticfiles/"

# PIPELINE_ENABLED = True
# PIPELINE = True

# STATICFILES_FINDERS = (
#     'pipeline.finders.FileSystemFinder',
#     'pipeline.finders.AppDirectoriesFinder',
#     'pipeline.finders.PipelineFinder',
#     'pipeline.finders.CachedFileFinder',
# )

from base import INSTALLED_APPS,MIDDLEWARE_CLASSES

INSTALLED_APPS = INSTALLED_APPS + ('lettuce.django',)

#MIDDLEWARE_CLASSES =  MIDDLEWARE_CLASSES  +  ('django_pdb.middleware.PdbMiddleware',)

EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

# AUTOMATED TESTS
LETTUCE_APPS = (
    'ui',
)

LETTUCE_TEST_SERVER = 'lettuce.django.server.DjangoServer'
LETTUCE_SERVER_PORT = 9000
LETTUCE_USE_TEST_DATABASE = True


