"""
Django settings for eiwa project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '4ipe_=c5!+%-uv%7_@4l@iz#jx=vlq75@d7ba-(h_%lnq51$7+'

# SECURITY WARNING: don't run with debug turned on in production!

ALLOWED_HOSTS = []

LOGIN_REDIRECT_URL = '/auth/loginSuccess'   # for decorator
LOGIN_URL = '/auth/login'
ADMIN_LANDING_PAGE = 'admindashboard:index'
CUSTOMER_LANDING_PAGE = 'ui:fields'

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',  
    'django.contrib.sites',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_extensions',
    'django_bootstrap_breadcrumbs',
    'widget_tweaks',
    'django.contrib.gis',
    'django_hstore',    
    'registration',
    'notification',
    'login',
    'admindashboard',
    'ui',
)


MIDDLEWARE_CLASSES = (
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

import django.conf.global_settings as DEFAULT_SETTINGS

TEMPLATE_CONTEXT_PROCESSORS = DEFAULT_SETTINGS.TEMPLATE_CONTEXT_PROCESSORS + (
    "django.core.context_processors.request",
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, "locale"),
    os.path.join(BASE_DIR+"/ui", "locale"),
    os.path.join(BASE_DIR+"/login", "locale"),
    os.path.join(BASE_DIR+"/admindashboard", "locale"),
)

ROOT_URLCONF = 'eiwa.urls'

WSGI_APPLICATION = 'eiwa.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/
LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SITE_ID = 1

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

# Static asset configuration
#PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))
#css/js/etc
STATIC_ROOT = 'staticfiles'
STATIC_URL = '/static/'

# user files (shp, tiles, tif)
MEDIA_ROOT = os.path.join(BASE_DIR, '../../userfiles')
MEDIA_URL = '/media/'

LOG_ROOT = os.path.join(BASE_DIR, "../../logs")

# django-registration-redux config
ACCOUNT_ACTIVATION_DAYS = 30

# notifications
NOTIFICATION_QUEUE_ALL = False

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "../static/"),
    )

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, '../templates/') ,
)
