import os
from base import *

ENVIRONMENT = os.getenv("DJANGO_ENVIRONMENT")

if ENVIRONMENT == "prod":
    from prod import *
elif ENVIRONMENT == "staging":
    from staging import *
elif ENVIRONMENT == "tests":
    from tests import *
elif ENVIRONMENT == "dev":
    from dev import *

else:
	try:
	    from local import *
	except ImportError:
		print("no environment defined")

