import os

# username and pass should be system properties
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'eiwaprod',
        'USER':  os.environ.get("EIWA_PROD_DB_USER", ''),
        'PASSWORD': os.environ.get("EIWA_PROD_DB_PASS", ''),
        'HOST':  '127.0.0.1',
        'PORT': '5432',
    }
}

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')


# this is not right
ALLOWED_HOSTS = ['*']


# STATIC_ROOT = "/var/www/app.eiwa.ag/static/"

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = os.environ.get("EIWA_PROD_EMAIL_USER", '')
EMAIL_HOST_PASSWORD = os.environ.get("EIWA_PROD_EMAIL_PASS", '')
EMAIL_PORT = 587

ADMINS = (('Marcela',  os.environ.get("EIWA_PROD_ADMIN_EMAIL", '')), )
SEND_BROKEN_LINK_EMAILS = True


from base import LOG_ROOT

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        # Include the default Django email handler for errors
        # This is what you'd get without configuring logging at all.
        'mail_admins': {
            'class': 'django.utils.log.AdminEmailHandler',
            'level': 'ERROR',
            # The emails are plain text by default -
            # HTML is nicer
            'include_html': True,
            'formatter': 'simple'
        },
        # Log to a text file that can be rotated by logrotate
        'logfile': {
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': LOG_ROOT + '/eiwa-ui.log',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        # Again, default Django configuration to email unhandled exceptions
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'WARNING',
            'propagate': True,
        },
        # Might as well log any errors anywhere else in Django
        'django': {
            'handlers': ['logfile'],
            'level': 'WARNING',
            'propagate': False,
        },
        # Your own app - this assumes all your logger names start with "ui."
        'ui': {
            'handlers': ['logfile'],
            'level': 'INFO',
            'propagate': False
        },
    },
}
