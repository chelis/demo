# STATIC_ROOT = "/home/chelis/development/freelance/Eiwa/proy/eiwa/staticfiles/"

# PIPELINE_ENABLED = True
# PIPELINE = True

# STATICFILES_FINDERS = (
#     'pipeline.finders.FileSystemFinder',
#     'pipeline.finders.AppDirectoriesFinder',
#     'pipeline.finders.PipelineFinder',
#     'pipeline.finders.CachedFileFinder',
# )


DEBUG = True

ALLOWED_HOSTS=['*']

TEMPLATE_DEBUG = True

from base import INSTALLED_APPS
INSTALLED_APPS = INSTALLED_APPS + ('debug_toolbar',)

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'eiwadevelopment',
        'USER': 'eiwadb',
        'PASSWORD': 'eiwadb',
        'HOST': ''
    }
}

EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

from base import LOG_ROOT

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        # Include the default Django email handler for errors
        # This is what you'd get without configuring logging at all.
        'mail_admins': {
            'class': 'django.utils.log.AdminEmailHandler',
            'level': 'ERROR',
             # But the emails are plain text by default - HTML is nicer
            'include_html': True,
            'formatter': 'simple'
        },
        # Log to a text file that can be rotated by logrotate
        'logfile': {
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': LOG_ROOT + '/eiwa-ui.log',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        # Again, default Django configuration to email unhandled exceptions
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'WARNING',
            'propagate': True,
        },
        # Might as well log any errors anywhere else in Django
        'django': {
            'handlers': ['logfile'],
            'level': 'WARNING',
            'propagate': False,
        },
        # Your own app - this assumes all your logger names start with "ui."
        'ui': {
            'handlers': ['logfile'],
            'level': 'INFO', # Or maybe INFO or DEBUG
            'propagate': False
        },
    },
}


