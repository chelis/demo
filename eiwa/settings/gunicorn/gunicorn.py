#import multiprocessing
#workers = multiprocessing.cpu_count() * 2 + 1

bind = "localhost:9000"
workers = 3
daemon = True
pidfile = "../logs/gunicorn.pid"
errorlog = "../logs/gunicorn_out.log"
accesslog = "../logs/gunicorn_access.log"
loglevel = "info"
raw_env=("DJANGO_ENVIRONMENT=local",)
proc_name = "gunicorn-local"
