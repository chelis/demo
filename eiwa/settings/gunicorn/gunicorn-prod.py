#import multiprocessing
#workers = multiprocessing.cpu_count() * 2 + 1

bind = "107.170.34.60:8090"
workers = 3
daemon = True
pidfile = "./logs/gunicorn.pid"
errorlog = "./logs/gunicorn_out.log"
accesslog = "./logs/gunicorn_access.log"
loglevel = "info"
raw_env = ("DJANGO_ENVIRONMENT=prod",)
proc_name = "gunicorn-prod"
