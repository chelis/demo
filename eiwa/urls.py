from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic.base import RedirectView
from django.contrib import admin
from django.core.urlresolvers import reverse_lazy
from django.conf.urls.static import static
from django.conf import settings

admin.autodiscover()



urlpatterns = patterns('',
    url(r'^ui/admin/', include(admin.site.urls)),
    url(r'^$', RedirectView.as_view(url=reverse_lazy('auth:login'))),
    url(r'^ui/', include('ui.urls', namespace='ui')),
    url(r'^auth/', include('login.urls', namespace='auth')),
    url(r'^admindashboard/', include('admindashboard.urls', namespace='admindashboard')),
)

# urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
