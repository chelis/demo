from django.core.management import call_command
from lettuce import before, after, world
from splinter.browser import Browser
from django.contrib.auth.models import User


@before.harvest
def initial_setup(server):
    call_command('syncdb', load_initial_data=False, interactive=False, verbosity=1)
    call_command('flush', interactive=False, verbosity=2)
    print(User.objects.all())
   # call_command('loaddata', 'all', verbosity=0)


@before.each_scenario
def flush_database(scenario):
    print(User.objects.all())
    call_command('flush', interactive=False, verbosity=0)
    print(User.objects.all())
   # call_command('loaddata', 'initialdata.json', verbosity=1)


@before.each_scenario
def prepare_browser(scenario):
    world.browser = Browser('phantomjs')


@after.each_scenario
def destroy_browser(scenario):
    world.browser.quit()
