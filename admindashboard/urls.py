from django.conf.urls import patterns, url
# from organizations.backends import invitation_backend
import views
from registration.backends.default.views import ActivationView
from django.contrib.auth.views import *

urlpatterns = patterns('',

    url(r'^$', views.index, name='index'),

    url(r'^customers/$', views.CustomersListView.as_view(), name='customers'),
    url(r'^customers/create$', views.CustomerCreateView.as_view(), name='customercreate'),
    url(r'^customers/(?P<pk>\d+)/update$', views.CustomerUpdateView.as_view(), name='customerupdate'),
    url(r'^customers/(?P<pk>[-_\w]+)/$', views.CustomerView.as_view(), name='customer'),
    url(r'^customers/(?P<pk>\d+)/delete$', views.CustomerDeleteView.as_view(), name='customerdelete'),

    url(r'^customers/(?P<pk>\d+)/fields/create$', views.CustomerFieldCreate.as_view(), name='customerfieldcreate'),
    url(r'^customers/(?P<customer_id>\d+)/fields/(?P<pk>\d+)/delete$', views.CustomerFieldDeleteView.as_view(), name='customerfielddelete'),
    url(r'^customers/(?P<customer_id>\d+)/fields/(?P<pk>\d+)/update$', views.CustomerFieldUpdateView.as_view(), name='customerfieldupdate'),
    url(r'^customers/(?P<customer_id>\d+)/fields/(?P<pk>\d+)$', views.CustomerFieldDetailView.as_view(), name='customerfield'),

    url(r'^users/$', views.UsersListView.as_view(), name='users'),
    url(r'^users/create$', views.UserCreateView.as_view(), name='usercreate'),
    url(r'^users/(?P<pk>\d+)/update$', views.UserUpdateView.as_view(), name='userupdate'),
    url(r'^users/(?P<pk>[-_\w]+)/$', views.UserDetailView.as_view(), name='user'),

    url(r'^users/(?P<pk>\d+)/delete$', views.UserDeleteView.as_view(), name='userdelete'),

    url(r'^imagetypes$', views.ImageTypeListView.as_view(), name='imagetypes'),
    url(r'^imagetypes/(?P<pk>[-_\w]+)/$', views.ImageTypeView.as_view(), name='imagetype'),
    url(r'^imagetypes/create$', views.ImageTypeCreateView.as_view(), name='imagetypecreate'),
    url(r'^imagetypes/(?P<pk>\d+)/update$', views.ImageTypeUpdateView.as_view(), name='imagetypeupdate'),
    url(r'^imagetypes/(?P<pk>\d+)/delete$', views.ImageTypeDeleteView.as_view(), name='imagetypedelete'),
)

