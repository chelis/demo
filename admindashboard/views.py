from django.shortcuts import (render, get_object_or_404)
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.contrib.auth.models import User
from django.contrib.sites.models import RequestSite

from registration.backends.default.views import RegistrationView
from braces.views import GroupRequiredMixin

from ui.models import Field, Customer, ImageType
from forms import *
from ui.models import BaseGeom


class AdminRequiredMixin(GroupRequiredMixin):
    group_required = u"Admin"


@login_required
def index(request):
    return render(request, 'admindashboard/adminDashboard.html')


class UsersListView(AdminRequiredMixin, ListView):
    model = User
    template_name = "admindashboard/users/user_list.html"


class UserCreateView(AdminRequiredMixin, RegistrationView):
    model = User
    template_name = "admindashboard/users/user_form.html"
    form_class = UserCreateForm

    def get_context_data(self, **kwargs):
        context = super(UserCreateView, self).get_context_data(**kwargs)
        if self.request.POST:
            context['organizationForm'] = OrganizationUserFormSet(self.request.POST)
        else:
            context['organizationForm'] = OrganizationUserFormSet()
        return context

    def register(self, request, **cleaned_data):
        context = self.get_context_data()
        organizationuser_form = context['organizationForm']
        if organizationuser_form.is_valid():
            #self.object = RegistrationManager.create_inactive_user
            #(form['username'], form['email'], form['password1'], None, False)
            context['site'] = RequestSite(request)
            self.object = super(UserCreateView, self).register(request, **cleaned_data)

            organizationuser_form.instance = self.object
            organizationuser_form.save()
            #self.object.registrationprofile.send_activation_email(None)
            return redirect(self.get_success_url(request, self.object))
        else:
            return self.render_to_response(self.get_context_data(
                form=form, organizationForm=organizationuser_form))

    def get_success_url(self, request, user):
        return reverse('admindashboard:users')


class UserDeleteView(AdminRequiredMixin, DeleteView):
    model = User
    template_name = "admindashboard/generic_confirm_delete.html"

    def get_success_url(self):
        return reverse('admindashboard:users')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(UserDeleteView, self).get_context_data(**kwargs)
        context['object_class'] = 'user'
        context['referer'] = "admindashboard:users"
        return context


class UserUpdateView(AdminRequiredMixin, UpdateView):
    model = User
    template_name = 'admindashboard/users/user_form.html'
    form_class = UserEditForm

    def get_context_data(self, **kwargs):
        context = super(UserUpdateView, self).get_context_data(**kwargs)
        if self.request.POST:
            context['organizationForm'] = OrganizationUserFormSet(
                self.request.POST, instance=self.object)
        else:
            context['organizationForm'] = OrganizationUserFormSet(instance=self.object)
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        organizationuser_form = context['organizationForm']
        if organizationuser_form.is_valid():
            self.object = form.save()
            organizationuser_form.save()
            return redirect(self.get_success_url())
        else:
            return self.render_to_response(
                self.get_context_data(form=form, organizationForm=organizationuser_form))

    def get_success_url(self):
        return reverse('admindashboard:users')


class UserDetailView(AdminRequiredMixin, DetailView):
    model = User
    template_name = 'admindashboard/users/user_detail.html'

    def get_success_url(self):
        return self.object.organizationuser.get_edit_url()

class CustomersListView(AdminRequiredMixin, ListView):
    queryset = Customer.objects.order_by('name')
    paginate_by = 10
    template_name = "admindashboard/customers/customer_list.html"

class CustomerView(AdminRequiredMixin, DetailView):
    model = Customer
    fields = ['name']
    template_name = "admindashboard/customers/customer_detail.html"


class CustomerCreateView(AdminRequiredMixin, CreateView):
    model = Customer
    fields = ['name']
    template_name = "admindashboard/customers/customer_form.html"

    def get_success_url(self):
        return self.object.get_edit_url()


class CustomerUpdateView(AdminRequiredMixin, UpdateView):
    model = Customer
    success_url = reverse_lazy('admindashboard:customers')
    fields = ['name']
    template_name = "admindashboard/customers/customer_form.html"


class CustomerDeleteView(AdminRequiredMixin, DeleteView):
    model = Customer
    success_url = reverse_lazy('admindashboard:customers')
    template_name = "admindashboard/generic_confirm_delete.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(CustomerDeleteView, self).get_context_data(**kwargs)
        context['object_class'] = 'customer'
        context['referer'] = "admindashboard:customers"
        # context['referer_params'] = None
        return context



class CustomerFieldCreate(AdminRequiredMixin, SuccessMessageMixin, CreateView):
    model = Field
    form_class = CustomerFieldForm
    template_name = 'admindashboard/customers/fields/field_form.html'
    success_message = _('Productive Unit "%(name)s" has been created.')

    def dispatch(self, *args, **kwargs):
        self.customer = get_object_or_404(Customer, pk=kwargs['pk'])
        return super(CustomerFieldCreate, self).dispatch(*args, **kwargs)

    def get_form(self, form_class):
        form = super(CustomerFieldCreate, self).get_form(form_class)
        form.instance.customer = self.customer
        return form

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(CustomerFieldCreate, self).get_context_data(**kwargs)
        context['customer'] = self.customer

        if self.request.POST:
            context['images_form'] = ProductiveUnitImageFormSet(self.request.POST,
                                                                self.request.FILES)
        else:
            context['images_form'] = ProductiveUnitImageFormSet()

        return context

    def form_valid(self, form):
        ctx = self.get_context_data()
        images = ctx['images_form']

        if images.is_valid():
            self.object = form.save()
            images.instance = self.object
            for image in images:
                image.instance.productive_unit = self.object
            images.save()

            warning_messages = BaseGeom.process_files(self.object)
            for message in warning_messages:
                messages.warning(self.request, message)

            return super(CustomerFieldCreate, self).form_valid(form)
        else:
            return super(CustomerFieldCreate, self).form_invalid(form)

    def get_success_url(self):
        return self.object.customer.get_edit_url()


class CustomerFieldDeleteView(AdminRequiredMixin, DeleteView):
    model = Field
    template_name = "admindashboard/generic_confirm_delete.html"

    def get_success_url(self):
        return self.object.customer.get_edit_url()

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(CustomerFieldDeleteView, self).get_context_data(**kwargs)
        context['object_class'] = 'field'
        context['referer'] = "admindashboard:customerupdate"
        context['referer_params'] = kwargs['object'].customer.id
        return context


class CustomerFieldUpdateView(AdminRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Field
    template_name = 'admindashboard/customers/fields/field_update_form.html'
    success_message = _('Productive Unit "%(name)s" has been updated.')

    def form_valid(self, form):
        ctx = self.get_context_data()
        images = ctx['images_form']

        if images.is_valid():
            self.object = form.save()
            images.instance = self.object
            obj = images.save()
            warning_messages = BaseGeom.process_files(self.object)
            for message in warning_messages:
                messages.warning(self.request, message)

            return super(CustomerFieldUpdateView, self).form_valid(form)
        else:
            return super(CustomerFieldUpdateView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(CustomerFieldUpdateView, self).get_context_data(**kwargs)

        if self.request.POST:
            context['images_form'] = ProductiveUnitImageFormSet(self.request.POST,
                                                                self.request.FILES,
                                                                instance=self.object)

        if 'images_form' not in context:
            context['images_form'] = ProductiveUnitImageFormSet(instance=self.object)

        return context

    def get_success_url(self):
        return self.object.customer.get_edit_url()


class CustomerFieldDetailView(AdminRequiredMixin, DetailView):
    model = Field
    template_name = 'admindashboard/customers/fields/field_detail.html'

    def get_success_url(self):
        return self.object.customer.get_edit_url()



class ImageTypeListView(AdminRequiredMixin, ListView):
    queryset = ImageType.objects.order_by('name')
    paginate_by = 10
    template_name = 'admindashboard/images/imagetype_list.html'


class ImageTypeView(AdminRequiredMixin, DetailView):
    model = ImageType
    fields = '__all__'
    template_name = 'admindashboard/images/imagetype_detail.html'


class ImageTypeCreateView(AdminRequiredMixin, CreateView):
    model = ImageType
    fields = '__all__'
    template_name = 'admindashboard/images/imagetype_form.html'

    def get_success_url(self):
        return self.object.get_edit_url()


class ImageTypeUpdateView(AdminRequiredMixin, UpdateView):
    model = ImageType
    success_url = reverse_lazy('imagetypes')
    fields = '__all__'
    template_name = 'admindashboard/images/imagetype_form.html'


class ImageTypeDeleteView(AdminRequiredMixin, DeleteView):
    model = ImageType
    success_url = reverse_lazy('imagetypes')
    template_name = "admindashboard/generic_confirm_delete.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(ImageTypeDeleteView, self).get_context_data(**kwargs)
        context['object_class'] = 'image type'
        context['referer'] = 'admindashboard:imagetypes'
        # context['referer_params'] = None
        return context
