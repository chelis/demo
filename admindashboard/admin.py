from django.contrib import admin
from ui.models import Customer
from ui.models import Field



class FieldInline(admin.StackedInline):
    model = Field
    extra = 0


class CustomerAdmin(admin.ModelAdmin):
    inlines = [FieldInline]



# admin.site.register(Customer,CustomerAdmin)

