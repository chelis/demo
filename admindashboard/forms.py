from django.forms import ModelForm, DateInput, DateField
from django.forms.models import inlineformset_factory, BaseInlineFormSet
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from functools import partial

date_format = '%d/%m/%Y'
DateInputCustom = partial(
    DateInput, format=(date_format), attrs={'class': 'datepicker'})

from ui.models import Field, ProductiveUnitImage, OrganizationUser

class CustomerFieldForm(ModelForm):

    class Meta:
        model = Field
        exclude = ('customer', 'delete', )


class UserCreateForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username',
                  'password1', 'password2', 'groups', )


class UserEditForm(UserChangeForm):

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username',
                  'password', 'groups', 'is_active', )


class NoEmptyInlineModelFormSet(BaseInlineFormSet):

    def __init__(self, *args, **kwargs):
        super(NoEmptyInlineModelFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


class ProductiveUnitImageForm(ModelForm):
    date_taken = DateField(
        widget=DateInputCustom(), input_formats=(date_format,))

    class Meta:
        model = ProductiveUnitImage
        fields = '__all__'

OrganizationUserFormSet = inlineformset_factory(User, OrganizationUser,
                                                can_delete=False,
                                                extra=1,
                                                formset=NoEmptyInlineModelFormSet)

ProductiveUnitImageFormSet = inlineformset_factory(Field, ProductiveUnitImage,
                                                   form=ProductiveUnitImageForm,
                                                   can_delete=True,
                                                   extra=1, max_num=1)
