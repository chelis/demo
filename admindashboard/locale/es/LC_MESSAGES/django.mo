��    .      �  =   �      �  6   �     (  
   /  	   :     D     S     \     a     i     p     x  @     (   �  :   �     $     -     9     F  	   R  
   \     g     m     {     �     �  
   �     �     �  
   �     �     �  	   �     �  
   �     �       	     ,     ,   D     q     x     �     �     �     �  k  �  8        H     O     _     y     �     �  	   �     �     �     �  D   �  (   	  @   6	     w	     	     �	     �	     �	     �	     �	  !   �	     

     
     0
     E
     L
     S
     V
     e
     
     �
     �
     �
     �
     �
     �
  .   �
  3        K     R     e     w  	   �     �             "             -   .                !   #   $   &   ,                                    +                                              *   
             	   '                         )   (   %              Are you sure you want to delete the %(object_class)s  Active Add Client Add Field Add Image Type Add User Back Borders Client Clients Create Create/Modify Customers, Assign Productive Pnits and Upload Data Create/Modify Image Categories to upload Create/Modify Users for each customer, Manage Permissions. Customer Description Display Name Edit Client Edit User Edit field Email Email Address Environment Field Details Fields First Name Groups Id Image Type Image Type Details Image Types Last Name Name New Client New Image Type New User New field Productive Unit "%(name)s" has been created. Productive Unit "%(name)s" has been updated. Update User Details Username Users borders groups Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-01-05 22:36+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Está seguro de que desea borrar el/la %(object_class)s  Activo Agregar Cliente Agregar Unidad Productiva Agregar tipo de imagen Agregar Usuario Volver Contornos Cliente Clientes Crear Crear/Modificar Clientes, Asignar Unidades Productivas y Subir Datos Crear/Modificar Categorías de Imágenes Crear/Modificar Usuarios para cada Cliente, Administrar Permisos Cliente Descripción Nombre a mostrar Editar Cliente Editar Usuario Editar Unidad Productiva Correo Electrónico Dirección de Correo Electrónico Parcelas Detalle de Unidad Productiva Unidades Productivas Nombre Grupos Id Tipo de imagen Detalle de tipo de imagen Tipos de imagen Apellido Nombre Nuevo Cliente Nuevo tipo de imagen Nuevo Usuario Nueva Unidad Productiva La Unidad Productiva "%(name)s" ha sido creada La Unidad Productiva "%(name)s" ha sido actualizada Grabar Detalle de Usuario Nombre de Usuario Usuarios Contornos grupos 