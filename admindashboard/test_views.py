from django.test import TestCase, override_settings
from django.core.urlresolvers import reverse
from django.test.client import Client
from datetime import date

from ui.models import *
from ui.business.managers import *
from django.conf import settings


@override_settings(MEDIA_ROOT='/tmp/django_test/')
class CustomerListViewTests(TestCase):

    """Contact list view tests."""

    fixtures = ['test_data.json']

    def setUp(self):
        self.client = Client()
        self.client.post(
            '/login/', {'username': 'testadmin', 'password': 'test'})

    def test_customers_in_context(self):
        response = self.client.get(reverse('customers'))

        self.assertEquals(len(list(response.context['customer_list'])), 5)

    def test_edit_customer(self):
        response = self.client.get(reverse('customerupdate', kwargs={'pk': 1}))

        self.assertEquals(response.context['customer'].id, 1)

    def tearDown(self):
        shutil.rmtree("/tmp/django_test/", ignore_errors=True)


@override_settings(MEDIA_ROOT='/tmp/django_test/',
                   CELERY_EAGER_PROPAGATES_EXCEPTIONS=True,
                   CELERY_ALWAYS_EAGER=True,
                   BROKER_BACKEND='memory')
class CustomerFieldCreateViewTest(TestCase):

    """Field creation tests."""

    fixtures = ['test_data.json']

    def setUp(self):
        self.it = ImageType.objects.get(name='Terrain')
        self.client = Client()
        self.client.post(
            '/login/', {'username': 'testadmin', 'password': 'test'})

    def test_customer_in_context(self):
        response = self.client.get(
            reverse('customerfieldcreate', kwargs={'pk': 3}))

        self.assertEqual(response.context['customer'].id, 3)

    def test_create_field_success(self):

        with open('ui/testsresources/parcelas.zip') as bucket_file,\
                open('ui/testsresources/contornos.zip') as borders_file:

            response = self.client.post(reverse('customerfieldcreate',
                                                kwargs={'pk': 3}),
                                        {'name': 'test_create_field_simple',
                                         'borders': 'b',
                                         'environment': 'b',
                                         'bordersFile': borders_file,
                                         'environmentsFile': bucket_file,
                                         'productiveunitimage_set-0-date_'
                                         'taken': '5/11/2014',
                                         'productiveunitimage_set-0-image_'
                                         'type': 'TR',
                                         'productiveunitimage_set-0-image':
                                         None,
                                         'productiveunitimage_set-TOTAL_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-INITIAL_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-MIN_NUM_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-MAX_NUM_'
                                         'FORMS': 1000}, follow=True)

            self.assertContains(response, 'test_create_field_simple')
            self.assertContains(response, 'static/contorno')
            self.assertContains(response, 'static/parcelas')
            self.assertContains(response, 'Productive Unit &quot;'
                                'test_create_field_simple&quot; has been'
                                ' created.')

            new_field = Field.objects.filter(
                name='test_create_field_simple')[0]
            self.assertIn('static/parcelas', str(new_field.environmentsFile))
            self.assertIn('static/contorno', str(new_field.bordersFile))
            self.assertEqual(new_field.environmentgeom_set.count(),  48)
            self.assertEqual(new_field.bordergeom_set.count(),  1)

    def test_create_field_invalid_extension(self):

        with open('ui/testsresources/test_no_shape.kml') as bucket_file,\
                open('ui/testsresources/contornos.zip') as borders_file:

            response = self.client.post(reverse('customerfieldcreate',
                                                kwargs={'pk': 3}),
                                        {'name': 'test_create_field_invalid_'
                                         'extension', 'borders': 'b',
                                         'environment': 'b',
                                         'bordersFile': borders_file,
                                         'environmentsFile': bucket_file,
                                         'productiveunitimage_set-0-date_'
                                         'taken': '5/11/2014',
                                         'productiveunitimage_set-0-image_'
                                         'type': 'TR',
                                         'productiveunitimage_set-0-image':
                                         None,
                                         'productiveunitimage_set-TOTAL_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-INITIAL_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-MIN_NUM_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-MAX_NUM_'
                                         'FORMS': 1000}, follow=True)

            self.assertContains(response, 'Importer Error in file')

            new_field = Field.objects.filter(
                name='test_create_field_invalid_extension')[0]
            self.assertIn(
                'static/test_no_shape', str(new_field.environmentsFile))
            self.assertIn('static/contorno', str(new_field.bordersFile))
            self.assertEqual(new_field.environmentgeom_set.count(),  0)
            self.assertEqual(new_field.bordergeom_set.count(),  1)

    def test_create_field_no_id(self):

        with open('ui/testsresources/test_no_id.zip') as bucket_file,\
                open('ui/testsresources/contornos.zip') as borders_file:

            response = self.client.post(reverse('customerfieldcreate',
                                                kwargs={'pk': 3}),
                                        {'name': 'test_create_field_no_id',
                                         'borders': 'b',
                                         'environment': 'b',
                                         'bordersFile': borders_file,
                                         'environmentsFile': bucket_file,
                                         'productiveunitimage_set-0-date_'
                                         'taken': '5/11/2014',
                                         'productiveunitimage_set-0-image_'
                                         'type': 'TR',
                                         'productiveunitimage_set-0-image':
                                         None,
                                         'productiveunitimage_set-TOTAL_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-INITIAL_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-MIN_NUM_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-MAX_NUM_'
                                         'FORMS': 1000}, follow=True)

            self.assertContains(response, 'Importer Error in file')

            new_field = Field.objects.filter(name='test_create_field_no_id')[0]
            self.assertIn('static/test_no_id', str(new_field.environmentsFile))
            self.assertIn('static/contorno', str(new_field.bordersFile))
            self.assertEqual(new_field.environmentgeom_set.count(),  0)
            self.assertEqual(new_field.bordergeom_set.count(),  1)

    def test_create_field_add_image(self):

        with open('ui/testsresources/parcelas.zip') as bucket_file, \
            open('ui/testsresources/contornos.zip') as borders_file, \
                open('ui/testsresources/Mosaico_Sony.tif') as terrain_image:

            response = self.client.post(reverse('customerfieldcreate',
                                                kwargs={'pk': 3}),
                                        {'name': 'test_create_field_image',
                                            'bordersFile': borders_file,
                                            'environmentsFile': bucket_file,
                                            'productiveunitimage_set-0-date_'
                                            'taken': '5/11/2014',
                                            'productiveunitimage_set-0-image_'
                                            'type': self.it.id,
                                            'productiveunitimage_set-0-image':
                                            terrain_image,
                                            'productiveunitimage_set-TOTAL_'
                                            'FORMS': 1,
                                            'productiveunitimage_set-INITIAL_'
                                            'FORMS': 0,
                                            'productiveunitimage_set-MIN_NUM_'
                                            'FORMS': 0,
                                            'productiveunitimage_set-MAX_NUM_'
                                            'FORMS': 1000}, follow=True)

            self.assertContains(response, 'test_create_field_image')
            self.assertContains(response, 'static/contorno')
            self.assertContains(response, 'static/parcelas')
            self.assertContains(response, 'Productive Unit &quot;test_create_'
                                'field_image&quot; has been created.')

            new_field = Field.objects.filter(name='test_create_field_image')[0]
            self.assertEqual(1, new_field.productiveunitimage_set.count())
            from django.core import mail
            self.assertEqual(len(mail.outbox), 1)

    def tearDown(self):
        shutil.rmtree("/tmp/django_test/", ignore_errors=True)


@override_settings(MEDIA_ROOT='/tmp/django_test/',
                   CELERY_EAGER_PROPAGATES_EXCEPTIONS=True,
                   CELERY_ALWAYS_EAGER=True,
                   BROKER_BACKEND='memory')
class CustomerFieldUpdateViewTest(TestCase):

    """Field creation tests."""

    fixtures = ['test_data.json']

    def setUp(self):
        self.it = ImageType.objects.get(name='Terrain')
        self.it_fc = ImageType.objects.get(name='False Color')
        self.client = Client()
        self.client.post(
            '/login/', {'username': 'testadmin', 'password': 'test'})

    def test_field_in_context(self):
        response = self.client.get(reverse('customerfieldupdate',
                                           kwargs={'customer_id': 3, 'pk': 4}))

        self.assertEqual(response.context['field'].id, 4)

    def test_update_field_success(self):

        with open('ui/testsresources/parcelas.zip') as bucket_file,\
                open('ui/testsresources/contornos.zip') as borders_file:

            response = self.client.post(reverse('customerfieldupdate',
                                                kwargs={'customer_id': 3,
                                                        'pk': 4}),
                                        {'customer': 3,
                                         'name': 'test_update_field_simple',
                                         'bordersFile': borders_file,
                                         'environmentsFile': bucket_file,
                                         'productiveunitimage_set-0-'
                                         'productive_unit': 4,
                                         'productiveunitimage_set-0-id': '',
                                         'productiveunitimage_set-0-date_'
                                         'taken': '',
                                         'productiveunitimage_set-0-image_'
                                         'type': self.it.id,
                                         'productiveunitimage_set-0-image':
                                         '',
                                         'productiveunitimage_set-TOTAL_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-INITIAL_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-MIN_NUM_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-MAX_NUM_'
                                         'FORMS': 1000}, follow=True)

            self.assertContains(response, 'test_update_field_simple')
            self.assertContains(response, 'static/contorno')
            self.assertContains(response, 'static/parcelas')
            self.assertContains(response, 'Productive Unit &quot;test_'
                                'update_field_simple&quot; has been updated.')

            new_field = Field.objects.filter(
                name='test_update_field_simple')[0]
            self.assertIn('static/parcelas', str(new_field.environmentsFile))
            self.assertIn('static/contorno', str(new_field.bordersFile))
            self.assertEqual(new_field.environmentgeom_set.count(),  48)
            self.assertEqual(new_field.bordergeom_set.count(),  1)

    def test_update_field_importer_error(self):

        with open('ui/testsresources/test_no_shape.kml') as bucket_file,\
                open('ui/testsresources/contornos.zip') as borders_file:

            response = self.client.post(reverse('customerfieldupdate',
                                                kwargs={'customer_id': 3,
                                                        'pk': 4}),
                                        {'customer': 3,
                                         'name':
                                         'test_update_field_importer_error',
                                         'bordersFile': borders_file,
                                         'environmentsFile': bucket_file,
                                         'productiveunitimage_set-0-'
                                         'productive_unit': 4,
                                         'productiveunitimage_set-0-id': '',
                                         'productiveunitimage_set-0-date_'
                                         'taken': '',
                                         'productiveunitimage_set-0-image_'
                                         'type': self.it.id,
                                         'productiveunitimage_set-0-image':
                                         '',
                                         'productiveunitimage_set-TOTAL_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-INITIAL_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-MIN_NUM_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-MAX_NUM_'
                                         'FORMS': 1000}, follow=True)

            self.assertContains(response, 'Importer Error in file')

            new_field = Field.objects.filter(
                name='test_update_field_importer_error')[0]
            self.assertIn(
                'static/test_no_shape', str(new_field.environmentsFile))
            self.assertIn('static/contorno', str(new_field.bordersFile))
            self.assertEqual(new_field.environmentgeom_set.count(),  0)
            self.assertEqual(new_field.bordergeom_set.count(),  1)

    def test_update_field_add_image(self):
        it = ImageType.objects.get(name='Terrain')
        with open('ui/testsresources/parcelas.zip') as bucket_file, \
            open('ui/testsresources/contornos.zip') as borders_file, \
                open('ui/testsresources/Mosaico_Sony_chico.tif') \
                as terrain_image:

            response = self.client.post(reverse('customerfieldupdate',
                                                kwargs={'customer_id': 3,
                                                        'pk': 4}),
                                        {'name': 'test_update_field_image',
                                            'customer': 3,
                                            'bordersFile': borders_file,
                                            'environmentsFile': bucket_file,
                                            'productiveunitimage_set-0-'
                                            'productive_unit': 4,
                                            'productiveunitimage_set-0-id': '',
                                            'productiveunitimage_set-0-date_'
                                            'taken': '5/11/2014',
                                            'productiveunitimage_set-0-image_'
                                            'type': self.it.id,
                                            'productiveunitimage_set-0-image':
                                            terrain_image,
                                            'productiveunitimage_set-TOTAL_'
                                            'FORMS': 1,
                                            'productiveunitimage_set-INITIAL_'
                                            'FORMS': 0,
                                            'productiveunitimage_set-MIN_NUM_'
                                            'FORMS': 0,
                                            'productiveunitimage_set-MAX_NUM_'
                                            'FORMS': 1000}, follow=True)

            new_field = Field.objects.filter(name='test_update_field_image')[0]
            self.assertContains(response, 'test_update_field_image')
            self.assertContains(response, 'static/contorno')
            self.assertContains(response, 'static/parcelas')
            self.assertContains(response, 'Productive Unit '
                                '&quot;test_update_field_image&quot; has been '
                                'updated.')
            self.assertEqual(1, new_field.productiveunitimage_set.count())
            path = settings.MEDIA_ROOT + \
                "/test/test_update_field_image/2014/11/05/originals"
            self.assertTrue(os.path.exists(path))
            path = settings.MEDIA_ROOT + \
                "/test/test_update_field_image/2014/11/05/tiles/terrain"
            self.assertTrue(os.path.exists(path))

    def test_update_field_update_image(self):

        with open('ui/testsresources/parcelas.zip') as bucket_file, \
                open('ui/testsresources/contornos.zip') as borders_file, \
            open('ui/testsresources/Mosaico_Sony_chico.tif') \
            as terrain_image_new,  \
                open('ui/testsresources/Mosaico_Sony.tif') as terrain_image:

            response = self.client.post(reverse('customerfieldcreate',
                                                kwargs={'pk': 3}),
                                        {'name': 'test_create_field_image',
                                         'bordersFile': borders_file,
                                         'environmentsFile': bucket_file,
                                         'productiveunitimage_set-0-date_'
                                         'taken': '5/11/2014',
                                         'productiveunitimage_set-0-image_'
                                         'type': self.it.id,
                                         'productiveunitimage_set-0-image':
                                         terrain_image,
                                         'productiveunitimage_set-TOTAL_'
                                         'FORMS': 1,
                                         'productiveunitimage_set-INITIAL_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-MIN_NUM_'
                                         'FORMS': 0,
                                         'productiveunitimage_set-MAX_NUM_'
                                         'FORMS': 1000}, follow=True)

            with open('ui/testsresources/parcelas.zip') as bucket_file,\
                    open('ui/testsresources/contornos.zip') as borders_file,\
                    open('ui/testsresources/Mosaico_Sony_chico.tif') \
                    as terrain_image_new,\
                    open('ui/testsresources/Mosaico_Sony.tif') \
                    as terrain_image:

                new_field = Field.objects.filter(
                    name='test_create_field_image')[0]
                response = self.client.post(reverse('customerfieldupdate',
                                                    kwargs={'customer_id': 3,
                                                            'pk': new_field.id}),
                                            {'name': 'test_update_field_image',
                                             'customer': 3,
                                             'bordersFile': borders_file,
                                             'environmentsFile': bucket_file,
                                             'productiveunitimage_set-0-'
                                             'productive_unit': new_field.id,
                                             'productiveunitimage_set-0-id':
                                             new_field.productiveunitimage_set.all()[
                                                 0].id,
                                             'productiveunitimage_set-0-date'
                                             '_taken': '7/11/2014',
                                             'productiveunitimage_set-0-image'
                                             '_type': self.it_fc.id,
                                             'productiveunitimage_set-0-image':
                                             terrain_image_new,
                                             'productiveunitimage_set-1-image_'
                                             'type': self.it.id,
                                             'productiveunitimage_set-1-'
                                             'productive_unit': new_field.id,
                                             'productiveunitimage_set-1-id':
                                             '',
                                             'productiveunitimage_set-TOTAL_'
                                             'FORMS': 1,
                                             'productiveunitimage_set-INITIAL_'
                                             'FORMS': 1,
                                             'productiveunitimage_set-MIN_NUM_'
                                             'FORMS': 0,
                                             'productiveunitimage_set-MAX_NUM_'
                                             'FORMS': 1000}, follow=True)

            new_field = Field.objects.get(pk=new_field.id)
            self.assertContains(response, 'test_update_field_image')
            self.assertContains(response, 'static/contorno')
            self.assertContains(response, 'static/parcelas')
            self.assertContains(response, 'Productive Unit &quot;test_update_'
                                'field_image&quot; has been updated.')

            self.assertEqual(1, new_field.productiveunitimage_set.count())
            image = new_field.productiveunitimage_set.all()[0]
            self.assertEqual(date(2014, 11, 7), image.date_taken)
            self.assertEqual('False Color', image.image_type.name)
            self.assertTrue('Mosaico_Sony_chico' in image.image.path)

    def tearDown(self):
        shutil.rmtree("/tmp/django_test/*", ignore_errors=True)


class ImageTypeTests(TestCase):

    """Image Type CRUD tests."""

    fixtures = ['test_data.json']

    def setUp(self):
        self.client = Client()
        self.client.post(
            '/login/', {'username': 'testadmin', 'password': 'test'})

    def test_image_type_list(self):
        response = self.client.get(reverse('imagetypes'))
        self.assertIsNotNone(response.context['imagetype_list'])
        self.assertEqual(4, len(response.context['imagetype_list']))

    def test_image_type_create(self):
        response = self.client.post(reverse('imagetypecreate'),
                                    {'name': 'test_image_type',
                                     'description': 'my description',
                                     'display_name': "test image type",
                                     }, follow=True)

        self.assertEqual(5, ImageType.objects.count())
        self.assertContains(response, 'Image Type')
