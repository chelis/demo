Application set up
=======================


Environments
---------------------

- Prod
- Local (dev)
- Test

Django settings -> <project root>/settings/[prod|local|tests].py 
Common stuff at <project root>/settings/base.py

Django settings module __init__.py  chooses environment based on DJANGO_ENVIRONMENT variable, which can be set to prod or tests, if it is not set then defaults to local.



Application set up
----------------------------


Database
--------


following packages need to be installed to be able to manipulate the database (for example, to drop all tables)

    sudo apt-get install libpq-dev
    sudo apt-get install python-dev

to install postgres server locally

sudo apt-get install postgresql postgresql-contrib

config postgres

sudo -u postgres psql postgres
sudo -u postgres createuser demodb
sudo -u postgres psql postgres
password
enter: demodb

change auth method

sudo vi /etc/postgresql/9.3/main/pg_hba.conf

change to: password
restart postgres

sudo /etc/init.d/postgresql restart



Application installation and startup
-------------------------------------------------------

There's a requirements file for pip by environment at requirements/

USER VIRTUALENV and VIRTUALENVWRAPPER.

workon <virtual_env>
pip install -r ./requirements/<env>.txt

local
-------

python manage.py syncdb
python manage.py runserver


prod
-------

./deploy/kill_server.sh
./deploy/backup_data.sh
./deploy/recreatedb_prod.sh
python manage.py collectstatic
./deploy/runserver_prod.sh



Update initial data:
-----------------------------

python manage.py dumpdata ui registration auth > ./ui/fixtures/initial_data.json

The contents of the file will be automatically uploaded to the database when running syncdb



Environments
____________

Bitbucket project:
  Holds the codebase.
  Branches:
    master -> production code
    development -> development branch with finished features

    work in progress is kept in feature branches